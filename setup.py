# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='GalaxieWeather',
    version='0.0.2',
    description='The Galaxie Weather',
    long_description=readme,
    author='Jérôme Ornech',
    author_email='tuxa@rtnp.org',
    url='https://github.com/Tuuux/galaxie-weather',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)