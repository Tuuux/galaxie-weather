#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Jérôme ORNECH alias "Tuux" <tuxa@rtnp.org> all rights reserved
__author__ = 'Tuux'


def convert_f2c(S):
    fahrenheit = float(S)
    # celsius = "%.2f" % (fahrenheit - 273.15)
    celsius = "%.2f" % (fahrenheit - 274.16)
    return celsius


def to_wind_description(speed):
    speed = float(speed)
    # Echelle de Beaufort meter/sec
    if 0.000 <= speed <= 0.299:
        return u'Calme'
    elif 0.300 <= speed <= 1.599:
        return u'Très légère brise'
    elif 1.600 <= speed <= 3.399:
        return u'Légère brise'
    elif 3.400 <= speed <= 5.499:
        return u'Petite brise'
    elif 5.500 <= speed <= 7.999:
        return u'Jolie brise'
    elif 8.000 <= speed <= 10.799:
        return u'Bonne brise'
    elif 10.800 <= speed <= 13.899:
        return u'Vent frais'
    elif 13.900 <= speed <= 17.199:
        return u'Grand frais'
    elif 17.200 <= speed <= 20.799:
        return u'Coup de vent'
    elif 20.800 <= speed <= 24.499:
        return u'Fort coup de vent'
    elif 24.500 <= speed <= 28.499:
        return u'Tempête'
    elif 28.500 <= speed <= 32.699:
        return u'Violente tempête'
    elif speed <= 32.700:
        return u'Ouragan'


def to_wind_beaufort(speed):
    speed = float(speed)
    # Echelle de Beaufort
    if 0.000 <= speed <= 0.299:
        return 0
    elif 0.300 <= speed <= 1.599:
        return 1
    elif 1.600 <= speed <= 3.399:
        return 2
    elif 3.400 <= speed <= 5.499:
        return 3
    elif 5.500 <= speed <= 7.999:
        return 4
    elif 8.000 <= speed <= 10.799:
        return 5
    elif 10.800 <= speed <= 13.899:
        return 6
    elif 13.900 <= speed <= 17.199:
        return 7
    elif 17.200 <= speed <= 20.799:
        return 8
    elif 20.800 <= speed <= 24.499:
        return 9
    elif 24.500 <= speed <= 28.499:
        return 10
    elif 28.500 <= speed <= 32.699:
        return 11
    elif speed <= 32.700:
        return 12


def to_direction(direction):
    direction = float(direction)
    if (348.75 <= direction <= 360) or (0 <= direction <= 11.24):
        return u'Nord             N  '
    elif 11.25 <= direction <= 33.74:
        return u'Nord-Nord-Est    NNE'
    elif 33.75 <= direction <= 56.24:
        return u'Nord-Est         NE '
    elif 56.25 <= direction <= 78.74:
        return u'Est-Nord-Est     ENE'
    elif 78.75 <= direction <= 101.24:
        return u'Est              E  '
    elif 101.25 <= direction <= 123.74:
        return u'Est-Sud-Est     ESE'
    elif 123.75 <= direction <= 146.24:
        return u'Sud-Est          SE '
    elif 146.25 <= direction <= 168.74:
        return u'Sud-Sud-Est      SSE'
    elif 168.75 <= direction <= 191.24:
        return u'Sud              S  '
    elif 191.25 <= direction <= 213.74:
        return u'Sud-Sud-Ouest   SSO'
    elif 213.75 <= direction <= 236.24:
        return u'Sud-Ouest       SO '
    elif 236.25 <= direction <= 258.74:
        return u'Ouest-Sud-Ouest OSO'
    elif 258.75 <= direction <= 281.24:
        return u'Ouest           O  '
    elif 281.25 <= direction <= 303.74:
        return u'Ouest-Nord-Ouest ONO'
    elif 303.75 <= direction <= 326.24:
        return u'Nord-Ouest      NO '
    elif 326.25 <= direction <= 348.74:
        return u'Nord-Nord-Ouest NNO'


def hpa_to_description(pressure):
    pressure = float(pressure)
    if 0 <= pressure <= 973.2506:
        return u'Tempête'
    elif 973.2507 <= pressure <= 986.5828:
        return u'Grande pluie'
    elif 986.5829 <= pressure <= 999.915:
        return u'Pluie ou vent'
    elif 999.916 <= pressure <= 1011.91398:
        return u'Variable'
    elif 1011.91399 <= pressure <= 1022.57974:
        return u'Beau temps'
    elif 1022.57975 <= pressure <= 1033.2455:
        return u'Beau fixe'
    elif 1033.2456 <= pressure <= 1046.5777:
        return u'Très sec'






