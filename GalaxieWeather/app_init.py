#!/usr/bin/env python
# -*- coding: utf-8 -*-

import yaml
import collections
import sys
import os
# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Jérôme ORNECH alias "Tuux" <tuxa@rtnp.org> all rights reserved
__author__ = 'Tuux'


class AppInit(object):
    def __init__(self, debug_level=0):
        file_location = u''
        file_location += u''.join(os.path.dirname(os.path.realpath(__file__)))
        file_location += u''.join(os.sep)

        background_dir = file_location
        background_dir += u'wallpapers'
        background_dir += u''.join(os.sep)

        font_dir = file_location
        font_dir += u'fonts'
        font_dir += u''.join(os.sep)

        weather_icons_font_file = font_dir
        weather_icons_font_file += u'weathericons-regular-webfont.ttf'

        icon_dir = file_location
        icon_dir += u'icons'
        icon_dir += u''.join(os.sep)

        self.background_dir = background_dir
        self.font_dir = font_dir
        self.weather_icons_font_file = weather_icons_font_file
        self.icon_dir = icon_dir
        self.debug_level = 0
