#!/usr/bin/env python
# -*- coding: utf-8 -*-
from PIL import Image, ImageDraw, ImageFont
import os
# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Jérôme ORNECH alias "Tuux" <tuxa@rtnp.org> all rights reserved
__author__ = 'Tuux'

# More information's: https://erikflowers.github.io/weather-icons/


def wi_to_unichr(icon_name):
    if icon_name == 'wi-day-sunny':
        return unichr(0xf00d)
    elif icon_name == 'wi-day-sunny':
        return unichr(0xf00d)
    elif icon_name == 'wi-day-cloudy':
        return unichr(0xf002)
    elif icon_name == 'wi-day-cloudy-gusts':
        return unichr(0xf000)
    elif icon_name == 'wi-day-cloudy-windy':
        return unichr(0xf001)
    elif icon_name == 'wi-day-fog':
        return unichr(0xf003)
    elif icon_name == 'wi-day-hail':
        return unichr(0xf004)
    elif icon_name == 'wi-day-haze':
        return unichr(0xf0b6)
    elif icon_name == 'wi-day-lightning':
        return unichr(0xf005)
    elif icon_name == 'wi-day-rain':
        return unichr(0xf008)
    elif icon_name == 'wi-day-rain-mix':
        return unichr(0xf006)
    elif icon_name == 'wi-day-rain-wind':
        return unichr(0xf007)
    elif icon_name == 'wi-day-showers':
        return unichr(0xf009)
    elif icon_name == 'wi-day-sleet':
        return unichr(0xf0b2)
    elif icon_name == 'wi-day-sleet-storm':
        return unichr(0xf068)
    elif icon_name == 'wi-day-snow':
        return unichr(0xf00a)
    elif icon_name == 'wi-day-snow-thunderstorm':
        return unichr(0xf06b)
    elif icon_name == 'wi-day-snow-wind':
        return unichr(0xf065)
    elif icon_name == 'wi-day-sprinkle':
        return unichr(0xf00b)
    elif icon_name == 'wi-day-storm-showers':
        return unichr(0xf00e)
    elif icon_name == 'wi-day-sunny-overcast':
        return unichr(0xf00c)
    elif icon_name == 'wi-day-thunderstorm':
        return unichr(0xf010)
    elif icon_name == 'wi-day-windy':
        return unichr(0xf085)
    elif icon_name == 'wi-solar-eclipse':
        return unichr(0xf06e)
    elif icon_name == 'wi-hot':
        return unichr(0xf072)
    elif icon_name == 'wi-day-cloudy-high':
        return unichr(0xf07d)
    elif icon_name == 'wi-day-light-wind':
        return unichr(0xf0c4)
    elif icon_name == 'wi-night-clear':
        return unichr(0xf02e)
    elif icon_name == 'wi-night-alt-cloudy':
        return unichr(0xf086)
    elif icon_name == 'wi-night-alt-cloudy-gusts':
        return unichr(0xf022)
    elif icon_name == 'wi-night-alt-cloudy-windy':
        return unichr(0xf023)
    elif icon_name == 'wi-night-alt-hail':
        return unichr(0xf024)
    elif icon_name == 'wi-night-alt-lightning':
        return unichr(0xf025)
    elif icon_name == 'wi-night-alt-rain':
        return unichr(0xf028)
    elif icon_name == 'wi-night-alt-rain-mix':
        return unichr(0xf026)
    elif icon_name == 'wi-night-alt-rain-wind':
        return unichr(0xf027)
    elif icon_name == 'wi-night-alt-showers':
        return unichr(0xf029)
    elif icon_name == 'wi-night-alt-sleet':
        return unichr(0xf0b4)
    elif icon_name == 'wi-night-alt-sleet-storm':
        return unichr(0xf06a)
    elif icon_name == 'wi-night-alt-snow':
        return unichr(0xf02a)
    elif icon_name == 'wi-night-alt-snow-thunderstorm':
        return unichr(0xf06d)
    elif icon_name == 'wi-night-alt-snow-wind':
        return unichr(0xf067)
    elif icon_name == 'wi-night-alt-sprinkle':
        return unichr(0xf02b)
    elif icon_name == 'wi-night-alt-storm-showers':
        return unichr(0xf02c)
    elif icon_name == 'wi-night-alt-thunderstorm':
        return unichr(0xf02d)
    elif icon_name == 'wi-night-cloudy':
        return unichr(0xf031)
    elif icon_name == 'wi-night-cloudy-gusts':
        return unichr(0xf02f)
    elif icon_name == 'wi-night-cloudy-windy':
        return unichr(0xf030)
    elif icon_name == 'wi-night-fog':
        return unichr(0xf04a)
    elif icon_name == 'wi-night-hail':
        return unichr(0xf032)
    elif icon_name == 'wi-night-lightning':
        return unichr(0xf033)
    elif icon_name == 'wi-night-partly-cloudy':
        return unichr(0xf083)
    elif icon_name == 'wi-night-rain':
        return unichr(0xf036)
    elif icon_name == 'wi-night-rain-mix':
        return unichr(0xf034)
    elif icon_name == 'wi-night-rain-wind':
        return unichr(0xf035)
    elif icon_name == 'wi-night-showers':
        return unichr(0xf037)
    elif icon_name == 'wi-night-sleet':
        return unichr(0xf0b3)
    elif icon_name == 'wi-night-sleet-storm':
        return unichr(0xf069)
    elif icon_name == 'wi-night-snow':
        return unichr(0xf038)
    elif icon_name == 'wi-night-snow-thunderstorm':
        return unichr(0xf06c)
    elif icon_name == 'wi-night-snow-wind':
        return unichr(0xf066)
    elif icon_name == 'wi-night-sprinkle':
        return unichr(0xf039)
    elif icon_name == 'wi-night-storm-showers':
        return unichr(0xf03a)
    elif icon_name == 'wi-night-thunderstorm':
        return unichr(0xf03b)
    elif icon_name == 'wi-lunar-eclipse':
        return unichr(0xf070)
    elif icon_name == 'wi-stars':
        return unichr(0xf077)
    elif icon_name == 'wi-storm-showers':
        return unichr(0xf01d)
    elif icon_name == 'wi-thunderstorm':
        return unichr(0xf01e)
    elif icon_name == 'wi-night-alt-cloudy-high':
        return unichr(0xf07e)
    elif icon_name == 'wi-night-cloudy-high':
        return unichr(0xf080)
    elif icon_name == 'wi-night-alt-partly-cloudy':
        return unichr(0xf081)
    elif icon_name == 'wi-cloud':
        return unichr(0xf041)
    elif icon_name == 'wi-cloudy':
        return unichr(0xf013)
    elif icon_name == 'wi-cloudy-gusts':
        return unichr(0xf011)
    elif icon_name == 'wi-cloudy-windy':
        return unichr(0xf012)
    elif icon_name == 'wi-fog':
        return unichr(0xf014)
    elif icon_name == 'wi-hail':
        return unichr(0xf015)
    elif icon_name == 'wi-rain':
        return unichr(0xf019)
    elif icon_name == 'wi-rain-mix':
        return unichr(0xf017)
    elif icon_name == 'wi-rain-wind':
        return unichr(0xf018)
    elif icon_name == 'wi-showers':
        return unichr(0xf01a)
    elif icon_name == 'wi-sleet':
        return unichr(0xf0b5)
    elif icon_name == 'wi-snow':
        return unichr(0xf01b)
    elif icon_name == 'wi-sprinkle':
        return unichr(0xf01c)
    elif icon_name == 'wi-storm-showers':
        return unichr(0xf01d)
    elif icon_name == 'wi-thunderstorm':
        return unichr(0xf01e)
    elif icon_name == 'wi-snow-wind':
        return unichr(0xf064)
    elif icon_name == 'wi-snow':
        return unichr(0xf01b)
    elif icon_name == 'wi-smog':
        return unichr(0xf074)
    elif icon_name == 'wi-smoke':
        return unichr(0xf062)
    elif icon_name == 'wi-lightning':
        return unichr(0xf016)
    elif icon_name == 'wi-raindrops':
        return unichr(0xf04e)
    elif icon_name == 'wi-raindrop':
        return unichr(0xf078)
    elif icon_name == 'wi-dust':
        return unichr(0xf063)
    elif icon_name == 'wi-snowflake-cold':
        return unichr(0xf076)
    elif icon_name == 'wi-windy':
        return unichr(0xf021)
    elif icon_name == 'wi-strong-wind':
        return unichr(0xf050)
    elif icon_name == 'wi-sandstorm':
        return unichr(0xf082)
    elif icon_name == 'wi-earthquake':
        return unichr(0xf0c6)
    elif icon_name == 'wi-fire':
        return unichr(0xf0c7)
    elif icon_name == 'wi-flood':
        return unichr(0xf07c)
    elif icon_name == 'wi-meteor':
        return unichr(0xf071)
    elif icon_name == 'wi-tsunami':
        return unichr(0xf0c5)
    elif icon_name == 'wi-volcano':
        return unichr(0xf0c8)
    elif icon_name == 'wi-hurricane':
        return unichr(0xf073)
    elif icon_name == 'wi-tornado':
        return unichr(0xf056)
    elif icon_name == 'wi-small-craft-advisory':
        return unichr(0xf0cc)
    elif icon_name == 'wi-gale-warning':
        return unichr(0xf0cd)
    elif icon_name == 'wi-storm-warning':
        return unichr(0xf0ce)
    elif icon_name == 'wi-hurricane-warning':
        return unichr(0xf0cf)
    elif icon_name == 'wi-wind-direction':
        return unichr(0xf0b1)
    elif icon_name == 'wi-alien':
        return unichr(0xf075)
    elif icon_name == 'wi-celsius':
        return unichr(0xf03c)
    elif icon_name == 'wi-fahrenheit':
        return unichr(0xf045)
    elif icon_name == 'wi-degrees':
        return unichr(0xf042)
    elif icon_name == 'wi-thermometer':
        return unichr(0xf055)
    elif icon_name == 'wi-thermometer-exterior':
        return unichr(0xf053)
    elif icon_name == 'wi-thermometer-internal':
        return unichr(0xf054)
    elif icon_name == 'wi-cloud-down':
        return unichr(0xf03d)
    elif icon_name == 'wi-cloud-up':
        return unichr(0xf040)
    elif icon_name == 'wi-cloud-refresh':
        return unichr(0xf03e)
    elif icon_name == 'wi-horizon':
        return unichr(0xf047)
    elif icon_name == 'wi-horizon-alt':
        return unichr(0xf046)
    elif icon_name == 'wi-sunrise':
        return unichr(0xf051)
    elif icon_name == 'wi-sunset':
        return unichr(0xf052)
    elif icon_name == 'wi-moonrise':
        return unichr(0xf0c9)
    elif icon_name == 'wi-moonset':
        return unichr(0xf0ca)
    elif icon_name == 'wi-refresh':
        return unichr(0xf04c)
    elif icon_name == 'wi-refresh-alt':
        return unichr(0xf04b)
    elif icon_name == 'wi-umbrella':
        return unichr(0xf084)
    elif icon_name == 'wi-barometer':
        return unichr(0xf079)
    elif icon_name == 'wi-humidity':
        return unichr(0xf07a)
    elif icon_name == 'wi-na':
        return unichr(0xf07b)
    elif icon_name == 'wi-train':
        return unichr(0xf0cb)
    elif icon_name == 'wi-moon-new':
        return unichr(0xf095)
    elif icon_name == 'wi-moon-waxing-crescent-1':
        return unichr(0xf096)
    elif icon_name == 'wi-moon-waxing-crescent-2':
        return unichr(0xf097)
    elif icon_name == 'wi-moon-waxing-crescent-3':
        return unichr(0xf098)
    elif icon_name == 'wi-moon-waxing-crescent-4':
        return unichr(0xf099)
    elif icon_name == 'wi-moon-waxing-crescent-5':
        return unichr(0xf09a)
    elif icon_name == 'wi-moon-waxing-crescent-6':
        return unichr(0xf09b)
    elif icon_name == 'wi-moon-first-quarter':
        return unichr(0xf09c)
    elif icon_name == 'wi-moon-waxing-gibbous-1':
        return unichr(0xf09d)
    elif icon_name == 'wi-moon-waxing-gibbous-2':
        return unichr(0xf09e)
    elif icon_name == 'wi-moon-waxing-gibbous-3':
        return unichr(0xf09f)
    elif icon_name == 'wi-moon-waxing-gibbous-4':
        return unichr(0xf0a0)
    elif icon_name == 'wi-moon-waxing-gibbous-5':
        return unichr(0xf0a1)
    elif icon_name == 'wi-moon-waxing-gibbous-6':
        return unichr(0xf0a2)
    elif icon_name == 'wi-moon-full':
        return unichr(0xf0a3)
    elif icon_name == 'wi-moon-waning-gibbous-1':
        return unichr(0xf0a4)
    elif icon_name == 'wi-moon-waning-gibbous-2':
        return unichr(0xf0a5)
    elif icon_name == 'wi-moon-waning-gibbous-3':
        return unichr(0xf0a6)
    elif icon_name == 'wi-moon-waning-gibbous-4':
        return unichr(0xf0a7)
    elif icon_name == 'wi-moon-waning-gibbous-5':
        return unichr(0xf0a8)
    elif icon_name == 'wi-moon-waning-gibbous-6':
        return unichr(0xf0a9)
    elif icon_name == 'wi-moon-third-quarter':
        return unichr(0xf0aa)
    elif icon_name == 'wi-moon-waning-crescent-1':
        return unichr(0xf0ab)
    elif icon_name == 'wi-moon-waning-crescent-2':
        return unichr(0xf0ac)
    elif icon_name == 'wi-moon-waning-crescent-3':
        return unichr(0xf0ad)
    elif icon_name == 'wi-moon-waning-crescent-4':
        return unichr(0xf0ae)
    elif icon_name == 'wi-moon-waning-crescent-5':
        return unichr(0xf0af)
    elif icon_name == 'wi-moon-waning-crescent-6':
        return unichr(0xf0b0)
    elif icon_name == 'wi-moon-alt-new':
        return unichr(0xf0eb)
    elif icon_name == 'wi-moon-alt-waxing-crescent-1':
        return unichr(0xf0d0)
    elif icon_name == 'wi-moon-alt-waxing-crescent-2':
        return unichr(0xf0d1)
    elif icon_name == 'wi-moon-alt-waxing-crescent-3':
        return unichr(0xf0d2)
    elif icon_name == 'wi-moon-alt-waxing-crescent-4':
        return unichr(0xf0d3)
    elif icon_name == 'wi-moon-alt-waxing-crescent-5':
        return unichr(0xf0d4)
    elif icon_name == 'wi-moon-alt-waxing-crescent-6':
        return unichr(0xf0d5)
    elif icon_name == 'wi-moon-alt-first-quarter':
        return unichr(0xf0d6)
    elif icon_name == 'wi-moon-alt-waxing-gibbous-1':
        return unichr(0xf0d7)
    elif icon_name == 'wi-moon-alt-waxing-gibbous-2':
        return unichr(0xf0d8)
    elif icon_name == 'wi-moon-alt-waxing-gibbous-3':
        return unichr(0xf0d9)
    elif icon_name == 'wi-moon-alt-waxing-gibbous-4':
        return unichr(0xf0da)
    elif icon_name == 'wi-moon-alt-waxing-gibbous-5':
        return unichr(0xf0db)
    elif icon_name == 'wi-moon-alt-waxing-gibbous-6':
        return unichr(0xf0dc)
    elif icon_name == 'wi-moon-alt-full':
        return unichr(0xf0dd)
    elif icon_name == 'wi-moon-alt-waning-gibbous-1':
        return unichr(0xf0de)
    elif icon_name == 'wi-moon-alt-waning-gibbous-2':
        return unichr(0xf0df)
    elif icon_name == 'wi-moon-alt-waning-gibbous-3':
        return unichr(0xf0e0)
    elif icon_name == 'wi-moon-alt-waning-gibbous-4':
        return unichr(0xf0e1)
    elif icon_name == 'wi-moon-alt-waning-gibbous-5':
        return unichr(0xf0e2)
    elif icon_name == 'wi-moon-alt-waning-gibbous-6':
        return unichr(0xf0e3)
    elif icon_name == 'wi-moon-alt-third-quarter':
        return unichr(0xf0e4)
    elif icon_name == 'wi-moon-alt-waning-crescent-1':
        return unichr(0xf0e5)
    elif icon_name == 'wi-moon-alt-waning-crescent-2':
        return unichr(0xf0e6)
    elif icon_name == 'wi-moon-alt-waning-crescent-3':
        return unichr(0xf0e7)
    elif icon_name == 'wi-moon-alt-waning-crescent-4':
        return unichr(0xf0e8)
    elif icon_name == 'wi-moon-alt-waning-crescent-5':
        return unichr(0xf0e9)
    elif icon_name == 'wi-moon-alt-waning-crescent-6':
        return unichr(0xf0ea)
    elif icon_name == 'wi-moon-0':
        return unichr(0xf095)
    elif icon_name == 'wi-moon-1':
        return unichr(0xf096)
    elif icon_name == 'wi-moon-2':
        return unichr(0xf097)
    elif icon_name == 'wi-moon-3':
        return unichr(0xf098)
    elif icon_name == 'wi-moon-4':
        return unichr(0xf099)
    elif icon_name == 'wi-moon-5':
        return unichr(0xf09a)
    elif icon_name == 'wi-moon-6':
        return unichr(0xf09b)
    elif icon_name == 'wi-moon-7':
        return unichr(0xf09c)
    elif icon_name == 'wi-moon-8':
        return unichr(0xf09d)
    elif icon_name == 'wi-moon-9':
        return unichr(0xf09e)
    elif icon_name == 'wi-moon-10':
        return unichr(0xf09f)
    elif icon_name == 'wi-moon-11':
        return unichr(0xf0a0)
    elif icon_name == 'wi-moon-12':
        return unichr(0xf0a1)
    elif icon_name == 'wi-moon-13':
        return unichr(0xf0a2)
    elif icon_name == 'wi-moon-14':
        return unichr(0xf0a3)
    elif icon_name == 'wi-moon-15':
        return unichr(0xf0a4)
    elif icon_name == 'wi-moon-16':
        return unichr(0xf0a5)
    elif icon_name == 'wi-moon-17':
        return unichr(0xf0a6)
    elif icon_name == 'wi-moon-18':
        return unichr(0xf0a7)
    elif icon_name == 'wi-moon-19':
        return unichr(0xf0a8)
    elif icon_name == 'wi-moon-20':
        return unichr(0xf0a9)
    elif icon_name == 'wi-moon-21':
        return unichr(0xf0aa)
    elif icon_name == 'wi-moon-22':
        return unichr(0xf0ab)
    elif icon_name == 'wi-moon-23':
        return unichr(0xf0ac)
    elif icon_name == 'wi-moon-24':
        return unichr(0xf0ad)
    elif icon_name == 'wi-moon-25':
        return unichr(0xf0ae)
    elif icon_name == 'wi-moon-26':
        return unichr(0xf0af)
    elif icon_name == 'wi-moon-27':
        return unichr(0xf0b0)
    elif icon_name == 'wi-time-1':
        return unichr(0xf08a)
    elif icon_name == 'wi-time-2':
        return unichr(0xf08b)
    elif icon_name == 'wi-time-3':
        return unichr(0xf08c)
    elif icon_name == 'wi-time-4':
        return unichr(0xf08d)
    elif icon_name == 'wi-time-5':
        return unichr(0xf08e)
    elif icon_name == 'wi-time-6':
        return unichr(0xf08f)
    elif icon_name == 'wi-time-7':
        return unichr(0xf090)
    elif icon_name == 'wi-time-8':
        return unichr(0xf091)
    elif icon_name == 'wi-time-9':
        return unichr(0xf092)
    elif icon_name == 'wi-time-10':
        return unichr(0xf093)
    elif icon_name == 'wi-time-11':
        return unichr(0xf094)
    elif icon_name == 'wi-time-12':
        return unichr(0xf089)
    elif icon_name == 'wi-direction-up':
        return unichr(0xf058)
    elif icon_name == 'wi-direction-up-right':
        return unichr(0xf057)
    elif icon_name == 'wi-direction-right':
        return unichr(0xf04d)
    elif icon_name == 'wi-direction-down-right':
        return unichr(0xf088)
    elif icon_name == 'wi-direction-down':
        return unichr(0xf044)
    elif icon_name == 'wi-direction-down-left':
        return unichr(0xf043)
    elif icon_name == 'wi-direction-left':
        return unichr(0xf048)
    elif icon_name == 'wi-direction-up-left':
        return unichr(0xf087)
    elif icon_name == 'wi-wind-beaufort-0':
        return unichr(0xf0b7)
    elif icon_name == 'wi-wind-beaufort-1':
        return unichr(0xf0b8)
    elif icon_name == 'wi-wind-beaufort-2':
        return unichr(0xf0b9)
    elif icon_name == 'wi-wind-beaufort-3':
        return unichr(0xf0ba)
    elif icon_name == 'wi-wind-beaufort-4':
        return unichr(0xf0bb)
    elif icon_name == 'wi-wind-beaufort-5':
        return unichr(0xf0bc)
    elif icon_name == 'wi-wind-beaufort-6':
        return unichr(0xf0bd)
    elif icon_name == 'wi-wind-beaufort-7':
        return unichr(0xf0be)
    elif icon_name == 'wi-wind-beaufort-8':
        return unichr(0xf0bf)
    elif icon_name == 'wi-wind-beaufort-9':
        return unichr(0xf0c0)
    elif icon_name == 'wi-wind-beaufort-10':
        return unichr(0xf0c1)
    elif icon_name == 'wi-wind-beaufort-11':
        return unichr(0xf0c2)
    elif icon_name == 'wi-wind-beaufort-12':
        return unichr(0xf0c3)
    elif icon_name == 'wi-yahoo-0':
        return unichr(0xf056)
    elif icon_name == 'wi-yahoo-1':
        return unichr(0xf00e)
    elif icon_name == 'wi-yahoo-2':
        return unichr(0xf073)
    elif icon_name == 'wi-yahoo-3':
        return unichr(0xf01e)
    elif icon_name == 'wi-yahoo-4':
        return unichr(0xf01e)
    elif icon_name == 'wi-yahoo-5':
        return unichr(0xf017)
    elif icon_name == 'wi-yahoo-6':
        return unichr(0xf017)
    elif icon_name == 'wi-yahoo-7':
        return unichr(0xf017)
    elif icon_name == 'wi-yahoo-8':
        return unichr(0xf015)
    elif icon_name == 'wi-yahoo-9':
        return unichr(0xf01a)
    elif icon_name == 'wi-yahoo-10':
        return unichr(0xf015)
    elif icon_name == 'wi-yahoo-11':
        return unichr(0xf01a)
    elif icon_name == 'wi-yahoo-12':
        return unichr(0xf01a)
    elif icon_name == 'wi-yahoo-13':
        return unichr(0xf01b)
    elif icon_name == 'wi-yahoo-14':
        return unichr(0xf00a)
    elif icon_name == 'wi-yahoo-15':
        return unichr(0xf064)
    elif icon_name == 'wi-yahoo-16':
        return unichr(0xf01b)
    elif icon_name == 'wi-yahoo-17':
        return unichr(0xf015)
    elif icon_name == 'wi-yahoo-18':
        return unichr(0xf017)
    elif icon_name == 'wi-yahoo-19':
        return unichr(0xf063)
    elif icon_name == 'wi-yahoo-20':
        return unichr(0xf014)
    elif icon_name == 'wi-yahoo-21':
        return unichr(0xf021)
    elif icon_name == 'wi-yahoo-22':
        return unichr(0xf062)
    elif icon_name == 'wi-yahoo-23':
        return unichr(0xf050)
    elif icon_name == 'wi-yahoo-24':
        return unichr(0xf050)
    elif icon_name == 'wi-yahoo-25':
        return unichr(0xf076)
    elif icon_name == 'wi-yahoo-26':
        return unichr(0xf013)
    elif icon_name == 'wi-yahoo-27':
        return unichr(0xf031)
    elif icon_name == 'wi-yahoo-28':
        return unichr(0xf002)
    elif icon_name == 'wi-yahoo-29':
        return unichr(0xf031)
    elif icon_name == 'wi-yahoo-30':
        return unichr(0xf002)
    elif icon_name == 'wi-yahoo-31':
        return unichr(0xf02e)
    elif icon_name == 'wi-yahoo-32':
        return unichr(0xf00d)
    elif icon_name == 'wi-yahoo-33':
        return unichr(0xf083)
    elif icon_name == 'wi-yahoo-34':
        return unichr(0xf00c)
    elif icon_name == 'wi-yahoo-35':
        return unichr(0xf017)
    elif icon_name == 'wi-yahoo-36':
        return unichr(0xf072)
    elif icon_name == 'wi-yahoo-37':
        return unichr(0xf00e)
    elif icon_name == 'wi-yahoo-38':
        return unichr(0xf00e)
    elif icon_name == 'wi-yahoo-39':
        return unichr(0xf00e)
    elif icon_name == 'wi-yahoo-40':
        return unichr(0xf01a)
    elif icon_name == 'wi-yahoo-41':
        return unichr(0xf064)
    elif icon_name == 'wi-yahoo-42':
        return unichr(0xf01b)
    elif icon_name == 'wi-yahoo-43':
        return unichr(0xf064)
    elif icon_name == 'wi-yahoo-44':
        return unichr(0xf00c)
    elif icon_name == 'wi-yahoo-45':
        return unichr(0xf00e)
    elif icon_name == 'wi-yahoo-46':
        return unichr(0xf01b)
    elif icon_name == 'wi-yahoo-47':
        return unichr(0xf00e)
    elif icon_name == 'wi-yahoo-3200':
        return unichr(0xf077)
    elif icon_name == 'wi-forecast-io-clear-day':
        return unichr(0xf00d)
    elif icon_name == 'wi-forecast-io-clear-night':
        return unichr(0xf02e)
    elif icon_name == 'wi-forecast-io-rain':
        return unichr(0xf019)
    elif icon_name == 'wi-forecast-io-snow':
        return unichr(0xf01b)
    elif icon_name == 'wi-forecast-io-sleet':
        return unichr(0xf0b5)
    elif icon_name == 'wi-forecast-io-wind':
        return unichr(0xf050)
    elif icon_name == 'wi-forecast-io-fog':
        return unichr(0xf014)
    elif icon_name == 'wi-forecast-io-cloudy':
        return unichr(0xf013)
    elif icon_name == 'wi-forecast-io-partly-cloudy-day':
        return unichr(0xf002)
    elif icon_name == 'wi-forecast-io-partly-cloudy-night':
        return unichr(0xf031)
    elif icon_name == 'wi-forecast-io-hail':
        return unichr(0xf015)
    elif icon_name == 'wi-forecast-io-thunderstorm':
        return unichr(0xf01e)
    elif icon_name == 'wi-forecast-io-tornado':
        return unichr(0xf056)
    elif icon_name == 'wi-wmo4680-0' or icon_name == 'wi-wmo4680-00':
        return unichr(0xf055)
    elif icon_name == 'wi-wmo4680-1' or icon_name == 'wi-wmo4680-01':
        return unichr(0xf013)
    elif icon_name == 'wi-wmo4680-2' or icon_name == 'wi-wmo4680-02':
        return unichr(0xf055)
    elif icon_name == 'wi-wmo4680-3' or icon_name == 'wi-wmo4680-03':
        return unichr(0xf013)
    elif icon_name == 'wi-wmo4680-4' or icon_name == 'wi-wmo4680-04':
        return unichr(0xf014)
    elif icon_name == 'wi-wmo4680-5' or icon_name == 'wi-wmo4680-05':
        return unichr(0xf014)
    elif icon_name == 'wi-wmo4680-10':
        return unichr(0xf014)
    elif icon_name == 'wi-wmo4680-11':
        return unichr(0xf014)
    elif icon_name == 'wi-wmo4680-12':
        return unichr(0xf016)
    elif icon_name == 'wi-wmo4680-18':
        return unichr(0xf050)
    elif icon_name == 'wi-wmo4680-20':
        return unichr(0xf014)
    elif icon_name == 'wi-wmo4680-21':
        return unichr(0xf017)
    elif icon_name == 'wi-wmo4680-22':
        return unichr(0xf017)
    elif icon_name == 'wi-wmo4680-23':
        return unichr(0xf019)
    elif icon_name == 'wi-wmo4680-24':
        return unichr(0xf01b)
    elif icon_name == 'wi-wmo4680-25':
        return unichr(0xf015)
    elif icon_name == 'wi-wmo4680-26':
        return unichr(0xf01e)
    elif icon_name == 'wi-wmo4680-27':
        return unichr(0xf063)
    elif icon_name == 'wi-wmo4680-28':
        return unichr(0xf063)
    elif icon_name == 'wi-wmo4680-29':
        return unichr(0xf063)
    elif icon_name == 'wi-wmo4680-30':
        return unichr(0xf014)
    elif icon_name == 'wi-wmo4680-31':
        return unichr(0xf014)
    elif icon_name == 'wi-wmo4680-32':
        return unichr(0xf014)
    elif icon_name == 'wi-wmo4680-33':
        return unichr(0xf014)
    elif icon_name == 'wi-wmo4680-34':
        return unichr(0xf014)
    elif icon_name == 'wi-wmo4680-35':
        return unichr(0xf014)
    elif icon_name == 'wi-wmo4680-40':
        return unichr(0xf017)
    elif icon_name == 'wi-wmo4680-41':
        return unichr(0xf01c)
    elif icon_name == 'wi-wmo4680-42':
        return unichr(0xf019)
    elif icon_name == 'wi-wmo4680-43':
        return unichr(0xf01c)
    elif icon_name == 'wi-wmo4680-44':
        return unichr(0xf019)
    elif icon_name == 'wi-wmo4680-45':
        return unichr(0xf015)
    elif icon_name == 'wi-wmo4680-46':
        return unichr(0xf015)
    elif icon_name == 'wi-wmo4680-47':
        return unichr(0xf01b)
    elif icon_name == 'wi-wmo4680-48':
        return unichr(0xf01b)
    elif icon_name == 'wi-wmo4680-50':
        return unichr(0xf01c)
    elif icon_name == 'wi-wmo4680-51':
        return unichr(0xf01c)
    elif icon_name == 'wi-wmo4680-52':
        return unichr(0xf019)
    elif icon_name == 'wi-wmo4680-53':
        return unichr(0xf019)
    elif icon_name == 'wi-wmo4680-54':
        return unichr(0xf076)
    elif icon_name == 'wi-wmo4680-55':
        return unichr(0xf076)
    elif icon_name == 'wi-wmo4680-56':
        return unichr(0xf076)
    elif icon_name == 'wi-wmo4680-57':
        return unichr(0xf01c)
    elif icon_name == 'wi-wmo4680-58':
        return unichr(0xf019)
    elif icon_name == 'wi-wmo4680-60':
        return unichr(0xf01c)
    elif icon_name == 'wi-wmo4680-61':
        return unichr(0xf01c)
    elif icon_name == 'wi-wmo4680-62':
        return unichr(0xf019)
    elif icon_name == 'wi-wmo4680-63':
        return unichr(0xf019)
    elif icon_name == 'wi-wmo4680-64':
        return unichr(0xf015)
    elif icon_name == 'wi-wmo4680-65':
        return unichr(0xf015)
    elif icon_name == 'wi-wmo4680-66':
        return unichr(0xf015)
    elif icon_name == 'wi-wmo4680-67':
        return unichr(0xf017)
    elif icon_name == 'wi-wmo4680-68':
        return unichr(0xf017)
    elif icon_name == 'wi-wmo4680-70':
        return unichr(0xf01b)
    elif icon_name == 'wi-wmo4680-71':
        return unichr(0xf01b)
    elif icon_name == 'wi-wmo4680-72':
        return unichr(0xf01b)
    elif icon_name == 'wi-wmo4680-73':
        return unichr(0xf01b)
    elif icon_name == 'wi-wmo4680-74':
        return unichr(0xf076)
    elif icon_name == 'wi-wmo4680-75':
        return unichr(0xf076)
    elif icon_name == 'wi-wmo4680-76':
        return unichr(0xf076)
    elif icon_name == 'wi-wmo4680-77':
        return unichr(0xf01b)
    elif icon_name == 'wi-wmo4680-78':
        return unichr(0xf076)
    elif icon_name == 'wi-wmo4680-80':
        return unichr(0xf019)
    elif icon_name == 'wi-wmo4680-81':
        return unichr(0xf01c)
    elif icon_name == 'wi-wmo4680-82':
        return unichr(0xf019)
    elif icon_name == 'wi-wmo4680-83':
        return unichr(0xf019)
    elif icon_name == 'wi-wmo4680-84':
        return unichr(0xf01d)
    elif icon_name == 'wi-wmo4680-85':
        return unichr(0xf017)
    elif icon_name == 'wi-wmo4680-86':
        return unichr(0xf017)
    elif icon_name == 'wi-wmo4680-87':
        return unichr(0xf017)
    elif icon_name == 'wi-wmo4680-89':
        return unichr(0xf015)
    elif icon_name == 'wi-wmo4680-90':
        return unichr(0xf016)
    elif icon_name == 'wi-wmo4680-91':
        return unichr(0xf01d)
    elif icon_name == 'wi-wmo4680-92':
        return unichr(0xf01e)
    elif icon_name == 'wi-wmo4680-93':
        return unichr(0xf01e)
    elif icon_name == 'wi-wmo4680-94':
        return unichr(0xf016)
    elif icon_name == 'wi-wmo4680-95':
        return unichr(0xf01e)
    elif icon_name == 'wi-wmo4680-96':
        return unichr(0xf01e)
    elif icon_name == 'wi-wmo4680-99':
        return unichr(0xf056)
    elif icon_name == 'wi-owm-200':
        return unichr(0xf01e)
    elif icon_name == 'wi-owm-201':
        return unichr(0xf01e)
    elif icon_name == 'wi-owm-202':
        return unichr(0xf01e)
    elif icon_name == 'wi-owm-210':
        return unichr(0xf016)
    elif icon_name == 'wi-owm-211':
        return unichr(0xf016)
    elif icon_name == 'wi-owm-212':
        return unichr(0xf016)
    elif icon_name == 'wi-owm-221':
        return unichr(0xf016)
    elif icon_name == 'wi-owm-230':
        return unichr(0xf01e)
    elif icon_name == 'wi-owm-231':
        return unichr(0xf01e)
    elif icon_name == 'wi-owm-232':
        return unichr(0xf01e)
    elif icon_name == 'wi-owm-300':
        return unichr(0xf01c)
    elif icon_name == 'wi-owm-301':
        return unichr(0xf01c)
    elif icon_name == 'wi-owm-302':
        return unichr(0xf019)
    elif icon_name == 'wi-owm-310':
        return unichr(0xf017)
    elif icon_name == 'wi-owm-311':
        return unichr(0xf019)
    elif icon_name == 'wi-owm-312':
        return unichr(0xf019)
    elif icon_name == 'wi-owm-313':
        return unichr(0xf01a)
    elif icon_name == 'wi-owm-314':
        return unichr(0xf019)
    elif icon_name == 'wi-owm-321':
        return unichr(0xf01c)
    elif icon_name == 'wi-owm-500':
        return unichr(0xf01c)
    elif icon_name == 'wi-owm-501':
        return unichr(0xf019)
    elif icon_name == 'wi-owm-502':
        return unichr(0xf019)
    elif icon_name == 'wi-owm-503':
        return unichr(0xf019)
    elif icon_name == 'wi-owm-504':
        return unichr(0xf019)
    elif icon_name == 'wi-owm-511':
        return unichr(0xf017)
    elif icon_name == 'wi-owm-520':
        return unichr(0xf01a)
    elif icon_name == 'wi-owm-521':
        return unichr(0xf01a)
    elif icon_name == 'wi-owm-522':
        return unichr(0xf01a)
    elif icon_name == 'wi-owm-531':
        return unichr(0xf01d)
    elif icon_name == 'wi-owm-600':
        return unichr(0xf01b)
    elif icon_name == 'wi-owm-601':
        return unichr(0xf01b)
    elif icon_name == 'wi-owm-602':
        return unichr(0xf0b5)
    elif icon_name == 'wi-owm-611':
        return unichr(0xf017)
    elif icon_name == 'wi-owm-612':
        return unichr(0xf017)
    elif icon_name == 'wi-owm-615':
        return unichr(0xf017)
    elif icon_name == 'wi-owm-616':
        return unichr(0xf017)
    elif icon_name == 'wi-owm-620':
        return unichr(0xf017)
    elif icon_name == 'wi-owm-621':
        return unichr(0xf01b)
    elif icon_name == 'wi-owm-622':
        return unichr(0xf01b)
    elif icon_name == 'wi-owm-701':
        return unichr(0xf01a)
    elif icon_name == 'wi-owm-711':
        return unichr(0xf062)
    elif icon_name == 'wi-owm-721':
        return unichr(0xf0b6)
    elif icon_name == 'wi-owm-731':
        return unichr(0xf063)
    elif icon_name == 'wi-owm-741':
        return unichr(0xf014)
    elif icon_name == 'wi-owm-761':
        return unichr(0xf063)
    elif icon_name == 'wi-owm-762':
        return unichr(0xf063)
    elif icon_name == 'wi-owm-771':
        return unichr(0xf011)
    elif icon_name == 'wi-owm-781':
        return unichr(0xf056)
    elif icon_name == 'wi-owm-800':
        return unichr(0xf00d)
    elif icon_name == 'wi-owm-801':
        return unichr(0xf011)
    elif icon_name == 'wi-owm-802':
        return unichr(0xf011)
    elif icon_name == 'wi-owm-803':
        return unichr(0xf012)
    elif icon_name == 'wi-owm-804':
        return unichr(0xf013)
    elif icon_name == 'wi-owm-900':
        return unichr(0xf056)
    elif icon_name == 'wi-owm-901':
        return unichr(0xf01d)
    elif icon_name == 'wi-owm-902':
        return unichr(0xf073)
    elif icon_name == 'wi-owm-903':
        return unichr(0xf076)
    elif icon_name == 'wi-owm-904':
        return unichr(0xf072)
    elif icon_name == 'wi-owm-905':
        return unichr(0xf021)
    elif icon_name == 'wi-owm-906':
        return unichr(0xf015)
    elif icon_name == 'wi-owm-957':
        return unichr(0xf050)
    elif icon_name == 'wi-owm-day-200':
        return unichr(0xf010)
    elif icon_name == 'wi-owm-day-201':
        return unichr(0xf010)
    elif icon_name == 'wi-owm-day-202':
        return unichr(0xf010)
    elif icon_name == 'wi-owm-day-210':
        return unichr(0xf005)
    elif icon_name == 'wi-owm-day-211':
        return unichr(0xf005)
    elif icon_name == 'wi-owm-day-212':
        return unichr(0xf005)
    elif icon_name == 'wi-owm-day-221':
        return unichr(0xf005)
    elif icon_name == 'wi-owm-day-230':
        return unichr(0xf010)
    elif icon_name == 'wi-owm-day-231':
        return unichr(0xf010)
    elif icon_name == 'wi-owm-day-232':
        return unichr(0xf010)
    elif icon_name == 'wi-owm-day-300':
        return unichr(0xf00b)
    elif icon_name == 'wi-owm-day-301':
        return unichr(0xf00b)
    elif icon_name == 'wi-owm-day-302':
        return unichr(0xf008)
    elif icon_name == 'wi-owm-day-310':
        return unichr(0xf008)
    elif icon_name == 'wi-owm-day-311':
        return unichr(0xf008)
    elif icon_name == 'wi-owm-day-312':
        return unichr(0xf008)
    elif icon_name == 'wi-owm-day-313':
        return unichr(0xf008)
    elif icon_name == 'wi-owm-day-314':
        return unichr(0xf008)
    elif icon_name == 'wi-owm-day-321':
        return unichr(0xf00b)
    elif icon_name == 'wi-owm-day-500':
        return unichr(0xf00b)
    elif icon_name == 'wi-owm-day-501':
        return unichr(0xf008)
    elif icon_name == 'wi-owm-day-502':
        return unichr(0xf008)
    elif icon_name == 'wi-owm-day-503':
        return unichr(0xf008)
    elif icon_name == 'wi-owm-day-504':
        return unichr(0xf008)
    elif icon_name == 'wi-owm-day-511':
        return unichr(0xf006)
    elif icon_name == 'wi-owm-day-520':
        return unichr(0xf009)
    elif icon_name == 'wi-owm-day-521':
        return unichr(0xf009)
    elif icon_name == 'wi-owm-day-522':
        return unichr(0xf009)
    elif icon_name == 'wi-owm-day-531':
        return unichr(0xf00e)
    elif icon_name == 'wi-owm-day-600':
        return unichr(0xf00a)
    elif icon_name == 'wi-owm-day-601':
        return unichr(0xf0b2)
    elif icon_name == 'wi-owm-day-602':
        return unichr(0xf00a)
    elif icon_name == 'wi-owm-day-611':
        return unichr(0xf006)
    elif icon_name == 'wi-owm-day-612':
        return unichr(0xf006)
    elif icon_name == 'wi-owm-day-615':
        return unichr(0xf006)
    elif icon_name == 'wi-owm-day-616':
        return unichr(0xf006)
    elif icon_name == 'wi-owm-day-620':
        return unichr(0xf006)
    elif icon_name == 'wi-owm-day-621':
        return unichr(0xf00a)
    elif icon_name == 'wi-owm-day-622':
        return unichr(0xf00a)
    elif icon_name == 'wi-owm-day-701':
        return unichr(0xf009)
    elif icon_name == 'wi-owm-day-711':
        return unichr(0xf062)
    elif icon_name == 'wi-owm-day-721':
        return unichr(0xf0b6)
    elif icon_name == 'wi-owm-day-731':
        return unichr(0xf063)
    elif icon_name == 'wi-owm-day-741':
        return unichr(0xf003)
    elif icon_name == 'wi-owm-day-761':
        return unichr(0xf063)
    elif icon_name == 'wi-owm-day-762':
        return unichr(0xf063)
    elif icon_name == 'wi-owm-day-781':
        return unichr(0xf056)
    elif icon_name == 'wi-owm-day-800':
        return unichr(0xf00d)
    elif icon_name == 'wi-owm-day-801':
        return unichr(0xf000)
    elif icon_name == 'wi-owm-day-802':
        return unichr(0xf000)
    elif icon_name == 'wi-owm-day-803':
        return unichr(0xf000)
    elif icon_name == 'wi-owm-day-804':
        return unichr(0xf00c)
    elif icon_name == 'wi-owm-day-900':
        return unichr(0xf056)
    elif icon_name == 'wi-owm-day-902':
        return unichr(0xf073)
    elif icon_name == 'wi-owm-day-903':
        return unichr(0xf076)
    elif icon_name == 'wi-owm-day-904':
        return unichr(0xf072)
    elif icon_name == 'wi-owm-day-906':
        return unichr(0xf004)
    elif icon_name == 'wi-owm-day-957':
        return unichr(0xf050)
    elif icon_name == 'wi-owm-night-200':
        return unichr(0xf02d)
    elif icon_name == 'wi-owm-night-201':
        return unichr(0xf02d)
    elif icon_name == 'wi-owm-night-202':
        return unichr(0xf02d)
    elif icon_name == 'wi-owm-night-210':
        return unichr(0xf025)
    elif icon_name == 'wi-owm-night-211':
        return unichr(0xf025)
    elif icon_name == 'wi-owm-night-212':
        return unichr(0xf025)
    elif icon_name == 'wi-owm-night-221':
        return unichr(0xf025)
    elif icon_name == 'wi-owm-night-230':
        return unichr(0xf02d)
    elif icon_name == 'wi-owm-night-231':
        return unichr(0xf02d)
    elif icon_name == 'wi-owm-night-232':
        return unichr(0xf02d)
    elif icon_name == 'wi-owm-night-300':
        return unichr(0xf02b)
    elif icon_name == 'wi-owm-night-301':
        return unichr(0xf02b)
    elif icon_name == 'wi-owm-night-302':
        return unichr(0xf028)
    elif icon_name == 'wi-owm-night-310':
        return unichr(0xf028)
    elif icon_name == 'wi-owm-night-311':
        return unichr(0xf028)
    elif icon_name == 'wi-owm-night-312':
        return unichr(0xf028)
    elif icon_name == 'wi-owm-night-313':
        return unichr(0xf028)
    elif icon_name == 'wi-owm-night-314':
        return unichr(0xf028)
    elif icon_name == 'wi-owm-night-321':
        return unichr(0xf02b)
    elif icon_name == 'wi-owm-night-500':
        return unichr(0xf02b)
    elif icon_name == 'wi-owm-night-501':
        return unichr(0xf028)
    elif icon_name == 'wi-owm-night-502':
        return unichr(0xf028)
    elif icon_name == 'wi-owm-night-503':
        return unichr(0xf028)
    elif icon_name == 'wi-owm-night-504':
        return unichr(0xf028)
    elif icon_name == 'wi-owm-night-511':
        return unichr(0xf026)
    elif icon_name == 'wi-owm-night-520':
        return unichr(0xf029)
    elif icon_name == 'wi-owm-night-521':
        return unichr(0xf029)
    elif icon_name == 'wi-owm-night-522':
        return unichr(0xf029)
    elif icon_name == 'wi-owm-night-531':
        return unichr(0xf02c)
    elif icon_name == 'wi-owm-night-600':
        return unichr(0xf02a)
    elif icon_name == 'wi-owm-night-601':
        return unichr(0xf0b4)
    elif icon_name == 'wi-owm-night-602':
        return unichr(0xf02a)
    elif icon_name == 'wi-owm-night-611':
        return unichr(0xf026)
    elif icon_name == 'wi-owm-night-612':
        return unichr(0xf026)
    elif icon_name == 'wi-owm-night-615':
        return unichr(0xf026)
    elif icon_name == 'wi-owm-night-616':
        return unichr(0xf026)
    elif icon_name == 'wi-owm-night-620':
        return unichr(0xf026)
    elif icon_name == 'wi-owm-night-621':
        return unichr(0xf02a)
    elif icon_name == 'wi-owm-night-622':
        return unichr(0xf02a)
    elif icon_name == 'wi-owm-night-701':
        return unichr(0xf029)
    elif icon_name == 'wi-owm-night-711':
        return unichr(0xf062)
    elif icon_name == 'wi-owm-night-721':
        return unichr(0xf0b6)
    elif icon_name == 'wi-owm-night-731':
        return unichr(0xf063)
    elif icon_name == 'wi-owm-night-741':
        return unichr(0xf04a)
    elif icon_name == 'wi-owm-night-761':
        return unichr(0xf063)
    elif icon_name == 'wi-owm-night-762':
        return unichr(0xf063)
    elif icon_name == 'wi-owm-night-781':
        return unichr(0xf056)
    elif icon_name == 'wi-owm-night-800':
        return unichr(0xf02e)
    elif icon_name == 'wi-owm-night-801':
        return unichr(0xf022)
    elif icon_name == 'wi-owm-night-802':
        return unichr(0xf022)
    elif icon_name == 'wi-owm-night-803':
        return unichr(0xf022)
    elif icon_name == 'wi-owm-night-804':
        return unichr(0xf086)
    elif icon_name == 'wi-owm-night-900':
        return unichr(0xf056)
    elif icon_name == 'wi-owm-night-902':
        return unichr(0xf073)
    elif icon_name == 'wi-owm-night-903':
        return unichr(0xf076)
    elif icon_name == 'wi-owm-night-904':
        return unichr(0xf072)
    elif icon_name == 'wi-owm-night-906':
        return unichr(0xf024)
    elif icon_name == 'wi-owm-night-957':
        return unichr(0xf050)
    elif icon_name == 'wi-wu-chanceflurries':
        return unichr(0xf064)
    elif icon_name == 'wi-wu-chancerain':
        return unichr(0xf019)
    elif icon_name == 'wi-wu-chancesleat':
        return unichr(0xf0b5)
    elif icon_name == 'wi-wu-chancesnow':
        return unichr(0xf01b)
    elif icon_name == 'wi-wu-chancetstorms':
        return unichr(0xf01e)
    elif icon_name == 'wi-wu-clear':
        return unichr(0xf00d)
    elif icon_name == 'wi-wu-cloudy':
        return unichr(0xf002)
    elif icon_name == 'wi-wu-flurries':
        return unichr(0xf064)
    elif icon_name == 'wi-wu-hazy':
        return unichr(0xf0b6)
    elif icon_name == 'wi-wu-mostlycloudy':
        return unichr(0xf002)
    elif icon_name == 'wi-wu-mostlysunny':
        return unichr(0xf00d)
    elif icon_name == 'wi-wu-partlycloudy':
        return unichr(0xf002)
    elif icon_name == 'wi-wu-partlysunny':
        return unichr(0xf00d)
    elif icon_name == 'wi-wu-rain':
        return unichr(0xf01a)
    elif icon_name == 'wi-wu-sleat':
        return unichr(0xf0b5)
    elif icon_name == 'wi-wu-snow':
        return unichr(0xf01b)
    elif icon_name == 'wi-wu-sunny':
        return unichr(0xf00d)
    elif icon_name == 'wi-wu-tstorms':
        return unichr(0xf01e)
    elif icon_name == 'wi-wu-unknown':
        return unichr(0xf00d)

def owm_icon_to_wi_icon(code):
    # 01
    if code == '01d':
        # wi-day-sunny
        return unichr(0xf00d)
    elif code == '01n':
        # wi-night-clear
        return unichr(0xf02e)
    # 02
    elif code == '02d':
        # wi-day-cloudy
        return unichr(0xf002)
    elif code == '02n':
        # wi-night-alt-cloudy
        return unichr(0xf086)
    # 03
    elif code == '03d':
        # wi-cloud
        return unichr(0xf041)
    elif code == '03n':
        # wi-cloud
        return unichr(0xf041)
    # 04
    elif code == '04d':
        # wi-cloudy
        return unichr(0xf013)
    elif code == '04n':
        # wi-cloudy
        return unichr(0xf013)
    # 09
    elif code == '09d':
        # wi-rain
        return unichr(0xf019)
    elif code == '09n':
        # wi-rain
        return unichr(0xf019)
    # 10
    elif code == '10d':
        # wi-day-rain
        return unichr(0xf008)
    elif code == '10n':
        # wi-night-alt-rain
        return unichr(0xf028)
    # 11
    elif code == '11d':
        # wi-thunderstorm
        return unichr(0xf01e)
    elif code == '11n':
        # wi-thunderstorm
        return unichr(0xf01e)
    # 13
    elif code == '13d':
        # wi-snow
        return unichr(0xf01b)
    elif code == '13n':
        # wi-snow
        return unichr(0xf01b)
    # 50
    elif code == '50d':
        # wi-fog
        return unichr(0xf014)
    elif code == '50n':
        # wi-fog
        return unichr(0xf014)


def moon_day_to_wi_icon(day):
    day = int(day)

    if day <= 1:
        # wi-moon-alt-new
        return unichr(0xf0eb)
    elif day == 2:
        # wi-moon-alt-waxing-crescent-1
        return unichr(0xf0d0)
    elif day == 3:
        # wi-moon-alt-waxing-crescent-2
        return unichr(0xf0d1)
    elif day == 4:
        # wi-moon-alt-waxing-crescent-3
        return unichr(0xf0d2)
    elif day == 5:
        # wi-moon-alt-waxing-crescent-4
        return unichr(0xf0d3)
    elif day == 6:
        # wi-moon-alt-waxing-crescent-5
        return unichr(0xf0d4)
    elif day == 7:
        # wi-moon-alt-waxing-crescent-6
        return unichr(0xf0d5)
    elif day == 8:
        # wi-moon-alt-first-quarter
        return unichr(0xf0d6)
    elif day == 9:
        # wi-moon-alt-waxing-gibbous-1
        return unichr(0xf0d7)
    elif day == 10:
        # wi-moon-alt-waxing-gibbous-2
        return unichr(0xf0d8)
    elif day == 11:
        # wi-moon-alt-waxing-gibbous-3
        return unichr(0xf0d9)
    elif day == 12:
        # wi-moon-alt-waxing-gibbous-4
        return unichr(0xf0da)
    elif day == 13:
        # wi-moon-alt-waxing-gibbous-5
        return unichr(0xf0db)
    elif day == 14:
        # wi-moon-alt-waxing-gibbous-6
        return unichr(0xf0dc)
    elif day == 15:
        # wi-moon-alt-full
        return unichr(0xf0dd)
    elif day == 16:
        # wi-moon-alt-waning-gibbous-1
        return unichr(0xf0de)
    elif day == 17:
        # wi-moon-alt-waning-gibbous-2
        return unichr(0xf0df)
    elif day == 18:
        # wi-moon-alt-waning-gibbous-3
        return unichr(0xf0e0)
    elif day == 19:
        # wi-moon-alt-waning-gibbous-4
        return unichr(0xf0e1)
    elif day == 20:
        # wi-moon-alt-waning-gibbous-5
        return unichr(0xf0e2)
    elif day == 21:
        # wi-moon-alt-waning-gibbous-6
        return unichr(0xf0e3)
    elif day == 22:
        # wi-moon-alt-third-quarter
        return unichr(0xf0e4)
    elif day == 23:
        # wi-moon-alt-waning-crescent-1
        return unichr(0xf0e5)
    elif day == 24:
        # wi-moon-alt-waning-crescent-2
        return unichr(0xf0e6)
    elif day == 25:
        # wi-moon-alt-waning-crescent-3
        return unichr(0xf0e7)
    elif day == 26:
        # wi-moon-alt-waning-crescent-4
        return unichr(0xf0e8)
    elif day == 27:
        # wi-moon-alt-waning-crescent-5
        return unichr(0xf0e9)
    elif day == 28:
        # wi-moon-alt-waning-crescent-6
        return unichr(0xf0ea)
    elif day >= 29:
        # wi-moon-alt-waning-crescent-6
        return unichr(0xf0ea)


def to_directional_arrows(direction):
    direction = float(direction)
    if (337.51 <= direction <= 360) or (0 <= direction <= 22.50):
        # wi-direction-up
        return unichr(0xf058)
    elif 22.51 <= direction <= 45.00:
        # wi-direction-up-right
        return unichr(0xf057)
    elif 45.01 <= direction <= 67.50:
        # wi-direction-up-right
        return unichr(0xf057)
    elif 67.51 <= direction <= 90.00:
        # wi-direction-right
        return unichr(0xf04d)
    elif 90.01 <= direction <= 112.50:
        # wi-direction-right
        return unichr(0xf04d)
    elif 112.51 <= direction <= 135.00:
        # wi-direction-down-right
        return unichr(0xf088)
    elif 135.01 <= direction <= 157.50:
        # wi-direction-down-right
        return unichr(0xf088)
    elif 157.50 <= direction <= 180.00:
        # wi-direction-down
        return unichr(0xf044)
    elif 180.01 <= direction <= 202.50:
        # wi-direction-down
        return unichr(0xf044)
    elif 202.51 <= direction <= 225.00:
        # wi-direction-down-left
        return unichr(0xf043)
    elif 225.01 <= direction <= 247.50:
        # wi-direction-down-left
        return unichr(0xf043)
    elif 247.51 <= direction <= 270.00:
        # wi-direction-left
        return unichr(0xf048)
    elif 270.01 <= direction <= 292.50:
        # wi-direction-left
        return unichr(0xf048)
    elif 292.51 <= direction <= 315.00:
        # wi-direction-up-left
        return unichr(0xf087)
    elif 315.01 <= direction <= 337.50:
        # wi-direction-up-left
        return unichr(0xf087)


def wind_degree_to_wi_img(direction, font, fill, debug_level=0):

    direction = float(direction)
    text_width, text_height = (font.getsize(wi_to_unichr('wi-wind-direction')))

    image = Image.new('RGBA', (text_width, text_width))
    mask = Image.new('L', image.size, color=0)
    image.putalpha(mask)
    draw = ImageDraw.Draw(image)
    draw.text((text_width/300, -(text_width/2) + text_width/25.5),
              wi_to_unichr('wi-wind-direction'),
              font=font,
              fill=fill)
    image = image.rotate(-direction, resample=Image.BICUBIC)

    return image
