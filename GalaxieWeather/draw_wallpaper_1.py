#!/usr/bin/env python
# -*- coding: utf-8 -*-

from GalaxieWeather.text_convert import convert_f2c
from GalaxieWeather.text_convert import to_wind_beaufort
from GalaxieWeather.ephem_utils import get_moon_phase_now
from GalaxieWeather.ephem_utils import to_moon_day
from GalaxieWeather.ephem_utils import get_sun_infos
from GalaxieWeather.ephem_utils import get_moon_infos
from GalaxieWeather.ephem_utils import get_day_or_night_for_wi
from GalaxieWeather.weather_icons import wi_to_unichr
from GalaxieWeather.weather_icons import moon_day_to_wi_icon
from GalaxieWeather.weather_icons import wind_degree_to_wi_img
import os
import locale
from PIL import Image, ImageDraw, ImageFont
import sys
import math

locale.setlocale(locale.LC_TIME, '')
# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Jérôme ORNECH alias "Tuux" <tuxa@rtnp.org> all rights reserved
__author__ = 'Tuux'


def per_to_px(per_width, per_height, image, debug_level=0):
    max_width, max_height = image.size
    # if debug_level >= 1:
    #     print int((max_width / 100.0) * float(per_width))
    #     print int((max_height / 100.0) * float(per_height))
    # Prepare return value
    returned_x_pos_value = int((max_width / 100.0) * float(per_width))
    returned_y_pos_value = int((max_height / 100.0) * float(per_height))

    xy_pos_list = list()
    xy_pos_list.append(returned_x_pos_value)
    xy_pos_list.append(returned_y_pos_value)
    return xy_pos_list


def distance2(a, b):
    return (a[0] - b[0]) * (a[0] - b[0]) + (a[1] - b[1]) * (a[1] - b[1]) + (a[2] - b[2]) * (a[2] - b[2])


def wallpaper_1(app_config, data, background_file=0, debug_level=0):
    # Variables init and collects
    font_dir = app_config.font_dir

    timestamp = float(data.api['weather_data_api']['dt']['value'])
    lat = unicode(data.api['weather_data_api']['coord_lat']['value'])
    lon = unicode(data.api['weather_data_api']['coord_lon']['value'])
    pressure = float(data.api['weather_data_api']['main_pressure']['value'])
    sun_info = get_sun_infos(timestamp, lat, lon, pressure, debug_level=debug_level)

    moon_info = get_moon_infos(timestamp, lat, lon, pressure, debug_level=debug_level)
    moon_azimut_text = unicode('{0:}'.format(moon_info['Azimut']))
    moon_azimut_text += u'°'

    temperature_value = unicode(u'%.2f' % float(convert_f2c(data.api['weather_data_api']['main_temp']['value'])))
    temperature_unit = unicode(data.api['weather_data_api']['main_temp']['unit'][str(data.api_unit)])
    if len(str(temperature_value)) == 6:
        temperature_text = unicode('{0: >0}'.format(temperature_value))
    elif len(str(temperature_value)) == 5:
        temperature_text = unicode('{0: >6}'.format(temperature_value))
    elif len(str(temperature_value)) == 4:
        temperature_text = unicode('{0: >7}'.format(temperature_value))
    elif len(str(temperature_value)) == 3:
        temperature_text = unicode('{0: >9}'.format(temperature_value))
    elif len(str(temperature_value)) == 2:
        temperature_text = unicode('{0: >9}'.format(temperature_value))
    elif len(str(temperature_value)) == 1:
        temperature_text = unicode('{0: >9}'.format(temperature_value))
    else:
        temperature_text = unicode('{0:}'.format(temperature_value))

    rain_3h_value = unicode(u'%.0f' % float(data.api['weather_data_api']['rain_3h']['value']))
    rain_3h_unit = unicode(data.api['weather_data_api']['rain_3h']['small_unit'][str(data.api_unit)])
    rain_3h_text = unicode('{0: >3}{1:}'.format(rain_3h_value, rain_3h_unit))

    humidity_value = unicode(u'%.0f' % float(data.api['weather_data_api']['main_humidity']['value']))
    humidity_unit = unicode(data.api['weather_data_api']['main_humidity']['unit'][str(data.api_unit)])
    humidity_text = unicode('{0:}{1:}'.format(humidity_value, humidity_unit))
    if len(str(humidity_text)) == 4:
        humidity_text = unicode('{0:}'.format(humidity_text))
    elif len(str(humidity_text)) == 3:
        humidity_text = unicode(

        )
    elif len(str(humidity_text)) == 2:
        humidity_text = unicode('{0: >6}'.format(humidity_text))
    elif len(str(humidity_text)) == 1:
        humidity_text = unicode('{0: >7}'.format(humidity_text))
    else:
        humidity_text = unicode('{0:}'.format(humidity_text))

    pressure_text = unicode(u'')
    pressure_text += unicode(u'%.0f' % float(data.api['weather_data_api']['main_pressure']['value']))
    pressure_text += unicode(data.api['weather_data_api']['main_pressure']['unit'][str(data.api_unit)])

    weather_description_text = unicode(data.api['weather_data_api']['weather'][0]['description']['value'])
    weather_description_text = u' '.join(word[0].upper() + word[1:] for word in weather_description_text.split())

    cloudness_value = unicode(data.api['weather_data_api']['clouds_all']['value'])
    cloudness_unit = unicode(data.api['weather_data_api']['clouds_all']['unit'][str(data.api_unit)])
    cloudness_text = unicode('{0:}{1:}'.format(cloudness_value, cloudness_unit))
    if len(str(cloudness_text)) == 4:
        cloudness_text = unicode('{0:}'.format(cloudness_text))
    elif len(str(cloudness_text)) == 3:
        cloudness_text = unicode('{0: >5}'.format(cloudness_text))
    elif len(str(cloudness_text)) == 2:
        cloudness_text = unicode('{0: >6}'.format(cloudness_text))
    elif len(str(cloudness_text)) == 1:
        cloudness_text = unicode('{0: >7}'.format(cloudness_text))
    else:
        cloudness_text = unicode('{0:}'.format(temperature_value))

    beaufort_value = unicode(to_wind_beaufort(data.api['weather_data_api']['wind_speed']['value']))

    wind_speed_value = unicode(u'%.2f' % data.api['weather_data_api']['wind_speed']['value'])
    wind_speed_unit = unicode(data.api['weather_data_api']['wind_speed']['small_unit'][str(data.api_unit)])
    wind_speed_text = unicode('{0:}{1:}'.format(wind_speed_value, wind_speed_unit))

    # Create a black image
    max_x = 1920
    max_y = 1200
    image_size = (max_x, max_y)
    image = Image.new('RGBA', image_size, (0, 0, 0))

    # Font Setting
    # https://erikflowers.github.io/weather-icons/
    font_weather_icons_moon = ImageFont.truetype(filename=app_config.weather_icons_font_file,
                                                 size=int(max_y / 6)
                                                 )
    font_weather_icons_giant = ImageFont.truetype(filename=app_config.weather_icons_font_file,
                                                  size=int(max_y / 10)
                                                  )
    font_weather_icons_direction = ImageFont.truetype(filename=app_config.weather_icons_font_file,
                                                      size=int(max_y / 16)
                                                      )
    font_weather_icons_big = ImageFont.truetype(filename=app_config.weather_icons_font_file,
                                                size=int(max_y / 20)
                                                )
    font_weather_icons_medium = ImageFont.truetype(filename=app_config.weather_icons_font_file,
                                                   size=int(max_y / 30)
                                                   )
    font_weather_icons_small = ImageFont.truetype(filename=app_config.weather_icons_font_file,
                                                  size=int(max_y / 40)
                                                  )
    font_weather_icons_tiny = ImageFont.truetype(filename=app_config.weather_icons_font_file,
                                                 size=int(max_y / 50)
                                                 )

    font_giant = ImageFont.truetype(filename=font_dir + 'DejaVuSans-Bold.ttf', size=int(max_y / 10))
    font_big = ImageFont.truetype(filename=font_dir + 'DejaVuSans-Bold.ttf', size=int(max_y / 20))
    font_small = ImageFont.truetype(filename=font_dir + 'DejaVuSans-Bold.ttf', size=int(max_y / 30))
    font_tiny = ImageFont.truetype(filename=font_dir + 'DejaVuSans.ttf', size=int(max_y / 40))

    day_or_night = str(get_day_or_night_for_wi(
        timestamp,
        lat,
        lon,
        pressure,
        debug_level=debug_level)
    )
    if day_or_night == 'day':
        inner_color = [117, 179, 204]
        outer_color = [30, 72, 130]
        font_color = (150, 150, 150, 127)
    else:
        inner_color = [15, 36, 65]
        outer_color = [0, 0, 0]
        font_color = (175, 175, 175, 127)

    if background_file:
        background_file_ext = os.path.splitext(background_file)[1]
        background_file_name = os.path.basename(os.path.splitext(background_file)[0])
        background_file_working_dir = os.path.realpath(os.path.dirname(background_file))
        background_file = os.path.join(background_file_working_dir, background_file_name + background_file_ext)
        background_image = Image.open(background_file).resize(image.size)
        if background_image.mode != 'RGBA':
            background_image = background_image.convert('RGBA')
        opacity_image = Image.new('RGBA', image.size, (0, 0, 0, 240))

        image.paste(Image.alpha_composite(background_image, opacity_image))
    else:
        # Draw the background
        for y in range(image_size[1]):
            for x in range(image_size[0]):
                # Find the distance to the center
                distance_to_center = math.sqrt((x - image_size[0] / 2) ** 2 + (y - image_size[1] / 2) ** 2)
                # Make it on a scale from 0 to 1
                distance_to_center = float(distance_to_center) / (math.sqrt(2) * image_size[0] / 2)
                # Calculate r, g, and b values
                r = outer_color[0] * distance_to_center + inner_color[0] * (1 - distance_to_center)
                g = outer_color[1] * distance_to_center + inner_color[1] * (1 - distance_to_center)
                b = outer_color[2] * distance_to_center + inner_color[2] * (1 - distance_to_center)
                # Place the pixel
                image.putpixel((x, y), (int(r), int(g), int(b), 150))

        # create a vertical gradient...
        gradient = Image.new('L', (1, 255))
        for y in range(255):
            gradient.putpixel((0, 254 - y), y / 2.5)
        # resize the gradient to the size of im...
        alpha = gradient.resize(image.size).transpose(Image.FLIP_TOP_BOTTOM).convert('RGBA')
        # put alpha in the alpha band of im...
        image = Image.alpha_composite(alpha, image.convert('RGBA'))

    drawer = ImageDraw.Draw(image)

    # Moonrise wi-sunrise
    drawer.text(per_to_px(90, 29.0, image),
                wi_to_unichr('wi-moonrise'),
                fill=font_color,
                font=font_weather_icons_small)
    drawer.text(per_to_px(93, 29.4, image),
                moon_info['Moonrise'],
                fill=font_color,
                font=font_tiny)

    # Moonset wi-horizon
    drawer.text(per_to_px(90, 32.4, image),
                wi_to_unichr('wi-moonset'),
                fill=font_color,
                font=font_weather_icons_small)
    drawer.text(per_to_px(93, 32.8, image),
                moon_info['Moonset'],
                fill=font_color,
                font=font_tiny)

    # Moon Azimut
    drawer.text(per_to_px(90.1, 35.8, image),
                wi_to_unichr('wi-night-clear'),
                fill=font_color,
                font=font_weather_icons_small)
    drawer.text(per_to_px(92, 36.2, image),
                moon_azimut_text,
                fill=font_color,
                font=font_tiny)
    if moon_info['Azimut'] < 0:
        moon_mount_or_not = u'wi-direction-up'
    else:
        moon_mount_or_not = u'wi-direction-down'
    drawer.text(per_to_px(96.7, 35.3, image),
                wi_to_unichr(moon_mount_or_not),
                fill=font_color,
                font=font_weather_icons_medium)

    # Moon
    drawer.text(per_to_px(90, 34, image),
                moon_day_to_wi_icon(to_moon_day(get_moon_phase_now())),
                fill=font_color,
                font=font_weather_icons_moon)

    # Sunrise wi-sunrise
    drawer.text(per_to_px(90, 53, image),
                wi_to_unichr('wi-sunrise'),
                fill=font_color,
                font=font_weather_icons_small)
    drawer.text(per_to_px(93, 53, image),
                sun_info['Sunrise'],
                fill=font_color,
                font=font_tiny)

    # Sunset wi-horizon
    drawer.text(per_to_px(90.25, 55.8, image),
                wi_to_unichr('wi-horizon'),
                fill=font_color,
                font=font_weather_icons_small)
    drawer.text(per_to_px(93, 56.4, image),
                sun_info['Sunset'],
                fill=font_color,
                font=font_tiny)

    # Delta
    drawer.text(per_to_px(90.25, 59.4, image),
                wi_to_unichr('wi-day-sunny'),
                fill=font_color,
                font=font_weather_icons_small)
    drawer.text(per_to_px(93.4, 59.8, image),
                sun_info['Delta'],
                fill=font_color,
                font=font_tiny)

    # The big Weather Icon
    wi_icon_name = str('wi-owm-')
    wi_icon_name += str(get_day_or_night_for_wi(
        timestamp,
        lat,
        lon,
        pressure,
        debug_level=debug_level)
    )
    wi_icon_name += str('-')
    wi_icon_name += str(data.api['weather_data_api']['weather'][0]['id']['value'])
    if debug_level >= 2:
        wi_icon_name_text = '{0: <21}: {1:}'.format('Weather Icon name', wi_icon_name)
        sys.stdout.write(unicode(wi_icon_name_text))
        sys.stdout.write(unicode('\n'))
        sys.stdout.flush()
    # Draw the big icon
    drawer.text(per_to_px(90, 63, image),
                wi_to_unichr(wi_icon_name),
                fill=font_color,
                font=font_weather_icons_giant)

    # Temperature
    icon_temperature_unit = u'wi-'
    if temperature_unit == 'Celsius':
        icon_temperature_unit += u'celsius'
    elif temperature_unit == 'Fahrenheit':
        icon_temperature_unit += u'fahrenheit'
    elif temperature_unit == 'Kelvin':
        icon_temperature_unit += u'degrees'
    if debug_level >= 2:
        icon_temperature_unit_text = '{0: <21}: {1:}'.format('Temperature Icon name', icon_temperature_unit)
        sys.stdout.write(unicode(icon_temperature_unit_text))
        sys.stdout.write(unicode('\n'))
        sys.stdout.flush()
    drawer.text(per_to_px(74, 76, image),
                temperature_text,
                fill=font_color,
                font=font_giant)
    drawer.text(per_to_px(96, 76.6, image),
                wi_to_unichr(icon_temperature_unit),
                fill=font_color,
                font=font_weather_icons_medium)
    drawer.text(per_to_px(96.1, 80.2, image),
                wi_to_unichr('wi-thermometer-exterior'),
                fill=font_color,
                font=font_weather_icons_medium)

    # Rain /3h - wi-umbrella
    drawer.text(per_to_px(81, 86, image),
                wi_to_unichr('wi-umbrella'),
                fill=font_color,
                font=font_weather_icons_tiny)
    drawer.text(per_to_px(82, 86, image),
                rain_3h_text,
                fill=font_color,
                font=font_tiny)

    # Humidity - wi-raindrop
    drawer.text(per_to_px(91, 85.9, image),
                wi_to_unichr('wi-raindrop'),
                fill=font_color,
                font=font_weather_icons_small)
    drawer.text(per_to_px(92.5, 86, image),
                humidity_text,
                fill=font_color,
                font=font_tiny)

    # Pressure - wi-barometer
    drawer.text(per_to_px(81, 89, image),
                wi_to_unichr('wi-barometer'),
                fill=font_color,
                font=font_weather_icons_small)
    drawer.text(per_to_px(82.3, 89.4, image),
                pressure_text,
                fill=font_color,
                font=font_tiny)

    # Cloudness wi-cloud
    drawer.text(per_to_px(90.6, 89.1, image),
                wi_to_unichr('wi-cloud'),
                fill=font_color,
                font=font_weather_icons_small)
    drawer.text(per_to_px(92.5, 89.4, image),
                cloudness_text,
                fill=font_color,
                font=font_tiny)

    # Wind Beaufort Scale - wi-strong-wind
    icon_beaufort_name = str('wi-wind-beaufort-')
    icon_beaufort_name += str(beaufort_value)
    drawer.text(per_to_px(81, 91, image),
                wi_to_unichr('wi-strong-wind'),
                fill=font_color,
                font=font_weather_icons_big)
    drawer.text(per_to_px(85, 92, image),
                beaufort_value,
                fill=font_color,
                font=font_big)

    # Wind degree icon
    image_wind_direction = wind_degree_to_wi_img(data.api['weather_data_api']['wind_deg']['value'],
                                                 font=font_weather_icons_direction,
                                                 fill=font_color,
                                                 debug_level=debug_level)
    image_wind_direction_tmp = Image.new('RGBA', image.size)
    image_wind_direction_tmp.paste(image_wind_direction, tuple(per_to_px(88.5, 92.7, image)))
    image.paste(Image.alpha_composite(image, image_wind_direction_tmp))

    drawer.text(per_to_px(92, 92.8, image),
                wind_speed_text,
                fill=font_color,
                font=font_tiny)

    if debug_level >= 2:
        sys.stdout.write(unicode('\n'))
        sys.stdout.write(unicode('\n'))
        sys.stdout.flush()

    return image
