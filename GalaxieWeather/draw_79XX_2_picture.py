#!/usr/bin/env python
# -*- coding: utf-8 -*-

from GalaxieWeather.text_convert import convert_f2c
from GalaxieWeather.text_convert import to_wind_beaufort
from GalaxieWeather.text_convert import hpa_to_description
from GalaxieWeather.ephem_utils import get_moon_phase_now
from GalaxieWeather.ephem_utils import to_moon_day
from GalaxieWeather.ephem_utils import get_sun_infos
from GalaxieWeather.ephem_utils import get_day_or_night_for_wi
from GalaxieWeather.weather_icons import wi_to_unichr
from GalaxieWeather.weather_icons import moon_day_to_wi_icon
from GalaxieWeather.weather_icons import to_directional_arrows
import os
import locale
from PIL import Image, ImageDraw, ImageFont
import sys

locale.setlocale(locale.LC_TIME, '')
# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Jérôme ORNECH alias "Tuux" <tuxa@rtnp.org> all rights reserved
__author__ = 'Tuux'


def draw_7960_2_info(app_config, data, debug_level=0):
    # Variables init and collects
    font_dir = app_config.font_dir

    timestamp = float(data.api['weather_data_api']['dt']['value'])
    lat = unicode(data.api['weather_data_api']['coord_lat']['value'])
    lon = unicode(data.api['weather_data_api']['coord_lon']['value'])
    pressure = float(data.api['weather_data_api']['main_pressure']['value'])
    sun_info = get_sun_infos(timestamp, lat, lon, pressure, debug_level=debug_level)

    temperature_value = unicode(u'%.0f' % float(convert_f2c(data.api['weather_data_api']['main_temp']['value'])))
    temperature_unit = unicode(data.api['weather_data_api']['main_temp']['unit'][str(data.api_unit)])
    temperature_text = unicode('{0: >3}'.format(temperature_value))

    rain_3h_value = unicode(u'%.0f' % float(data.api['weather_data_api']['rain_3h']['value']))
    rain_3h_unit = unicode(data.api['weather_data_api']['rain_3h']['small_unit'][str(data.api_unit)])
    rain_3h_text = unicode('{0: >3}{1:}'.format(rain_3h_value, rain_3h_unit))

    humidity_value = unicode(u'%.0f' % float(data.api['weather_data_api']['main_humidity']['value']))
    humidity_unit = unicode(data.api['weather_data_api']['main_humidity']['unit'][str(data.api_unit)])
    humidity_text = unicode('{0: >3}{1:}'.format(humidity_value, humidity_unit))

    pressure_text = unicode(u'')
    pressure_text += hpa_to_description(float(data.api['weather_data_api']['main_pressure']['value']))

    weather_description_text = unicode(data.api['weather_data_api']['weather'][0]['description']['value'])
    weather_description_text = u' '.join(word[0].upper() + word[1:] for word in weather_description_text.split())

    cloudness_value = unicode(data.api['weather_data_api']['clouds_all']['value'])
    cloudness_unit = unicode(data.api['weather_data_api']['clouds_all']['unit'][str(data.api_unit)])
    cloudness_text = unicode('{0: >3}{1:}'.format(cloudness_value, cloudness_unit))

    beaufort_value = unicode(to_wind_beaufort(data.api['weather_data_api']['wind_speed']['value']))

    # Font Setting
    # https://erikflowers.github.io/weather-icons/
    font_weathericons_moon = ImageFont.truetype(filename=app_config.weather_icons_font_file, size=36)
    font_weathericons_giant = ImageFont.truetype(filename=app_config.weather_icons_font_file, size=32)
    font_weathericons_big = ImageFont.truetype(filename=app_config.weather_icons_font_file, size=16)
    font_weathericons_medium = ImageFont.truetype(filename=app_config.weather_icons_font_file, size=13)
    font_weathericons_small = ImageFont.truetype(filename=app_config.weather_icons_font_file, size=10)
    font_weathericons_tiny = ImageFont.truetype(filename=app_config.weather_icons_font_file, size=8)

    font_giant = ImageFont.truetype(filename=font_dir + 'DejaVuSans-Bold.ttf', size=25)
    font_big = ImageFont.truetype(filename=font_dir + 'DejaVuSans-Bold.ttf', size=15)
    font_small = ImageFont.truetype(filename=font_dir + 'DejaVuSans-Bold.ttf', size=14)
    font_tiny = ImageFont.truetype(filename=font_dir + 'DejaVuSans.ttf', size=9)

    # Create a black image
    max_x = 125
    max_y = 60
    line_number = 7
    line_spacing = 0.1
    line_h_size = (max_y / (line_number - line_spacing))

    image = Image.new('RGB', (max_x, max_y), (255, 255, 255))
    drawer = ImageDraw.Draw(image)

    # The big Weather Icon
    wi_icon_name = str('wi-owm-')
    wi_icon_name += str(get_day_or_night_for_wi(
        timestamp,
        lat,
        lon,
        pressure,
        debug_level=debug_level)
    )
    wi_icon_name += str('-')
    wi_icon_name += str(data.api['weather_data_api']['weather'][0]['id']['value'])

    if debug_level >= 2:
        wi_icon_name_text = '{0: <21}: {1:}'.format('Weather Icon name', wi_icon_name)
        sys.stdout.write(unicode(wi_icon_name_text))
        sys.stdout.write(unicode('\n'))
        sys.stdout.flush()
    # Draw the big icon
    drawer.text((0, 0),
                wi_to_unichr(wi_icon_name),
                fill='black',
                font=font_weathericons_giant)

    # Temperature
    icon_temperature_unit = u'wi-'
    if temperature_unit == 'Celsius':
        icon_temperature_unit += u'celsius'
    elif temperature_unit == 'Fahrenheit':
        icon_temperature_unit += u'fahrenheit'
    elif temperature_unit == 'Kelvin':
        icon_temperature_unit += u'degrees'
    if debug_level >= 2:
        icon_temperature_unit_text = '{0: <21}: {1:}'.format('Temperature Icon name', icon_temperature_unit)
        sys.stdout.write(unicode(icon_temperature_unit_text))
        sys.stdout.write(unicode('\n'))
        sys.stdout.flush()
    drawer.text((int(max_x / 3.3) * 1 + 3, int(line_h_size) + 4), temperature_text, fill='black', font=font_giant)
    drawer.text((int(max_x / 3.3) * 1 + 45, int(line_h_size) + 4),
                wi_to_unichr(icon_temperature_unit),
                fill='black',
                font=font_weathericons_medium)

    # Rain /3h - wi-umbrella
    # drawer.text((int(max_x / 1.50) - 12, -2),
    #             wi_to_unichr('wi-umbrella'),
    #             fill='black',
    #             font=font_weathericons_tiny)
    # drawer.text((int(max_x / 1.50), 0), rain_3h_text, fill='black', font=font_tiny)

    # Humidity - wi-raindrop
    # drawer.text((int(max_x / 3) - 4, -2),
    #             wi_to_unichr('wi-raindrop'),
    #             fill='black',
    #             font=font_weathericons_small)
    # drawer.text((int(max_x / 3) + 2, 0), humidity_text, fill='black', font=font_tiny)

    # Moon
    drawer.text((96, int(line_h_size) - 9),
                moon_day_to_wi_icon(to_moon_day(get_moon_phase_now())),
                fill='black',
                font=font_weathericons_moon)

    # Pressure - wi-barometer
    # drawer.text((int(max_x / 3) - 4, int(line_h_size * 1) + 1), pressure_text, fill='black', font=font_tiny)

    # Wind Beaufort Scale - wi-strong-wind
    icon_beaufort_name = str('wi-wind-beaufort-')
    icon_beaufort_name += str(beaufort_value)
    drawer.text((int(max_x / 2.4), 0 - 5),
                wi_to_unichr('wi-strong-wind'),
                fill='black',
                font=font_weathericons_big)
    drawer.text((int(max_x / 2.4) + 21, 0 - 1),
                beaufort_value,
                fill='black',
                font=font_big)
    # drawer.text((int(max_x / 1.55) - 8, int(line_h_size * 3) - 5),
    #             wi_to_unichr('wi-strong-wind'),
    #             fill='black',
    #             font=font_weathericons_small)
    # drawer.text((int(max_x / 1.55) + 6, int(line_h_size * 3) - 2),
    #             beaufort_value,
    #             fill='black',
    #             font=font_tiny)

    # Wind direction
    drawer.text((int(max_x / 1.5), -15),
                to_directional_arrows('%.2f' % data.api['weather_data_api']['wind_deg']['value']),
                fill='black',
                font=font_weathericons_giant)

    # Cloudness wi-cloud
    # drawer.text((int(max_x / 3) - 9, int(line_h_size * 5) - 5),
    #             wi_to_unichr('wi-cloud'),
    #             fill='black',
    #             font=font_weathericons_small)
    # drawer.text((int(max_x / 3) + 2, int(line_h_size * 5) - 2),
    #             cloudness_text,
    #             fill='black',
    #             font=font_tiny)

    # Weather description
    drawer.text((0, int(line_h_size * 5) - 2),
                weather_description_text,
                fill='black',
                font=font_tiny)

    # Sunrise wi-sunrise
    drawer.text((0, int(line_h_size * 6) - 3),
                wi_to_unichr('wi-sunrise'),
                fill='black',
                font=font_weathericons_small)
    drawer.text((13, int(line_h_size * 6) ),
                sun_info['Sunrise'],
                fill='black',
                font=font_tiny)

    # Sunset wi-horizon
    drawer.text((50, int(line_h_size * 6) - 3),
                wi_to_unichr('wi-horizon'),
                fill='black',
                font=font_weathericons_small)
    drawer.text((60, int(line_h_size * 6)),
                sun_info['Sunset'],
                fill='black',
                font=font_tiny)

    # Delta
    drawer.text((95, int(line_h_size * 6)),
                sun_info['Delta'],
                fill='black',
                font=font_tiny)

    if debug_level >= 2:
        sys.stdout.write(unicode('\n'))
        sys.stdout.write(unicode('\n'))
        sys.stdout.flush()

    return image
