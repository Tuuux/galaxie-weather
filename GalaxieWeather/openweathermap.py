#!/usr/bin/env python
# -*- coding: utf-8 -*-

from urllib2 import urlopen, URLError, HTTPError
import json
import yaml
import collections
import sys
import os
# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Jérôme ORNECH alias "Tuux" <tuxa@rtnp.org> all rights reserved
__author__ = 'Tuux'


class Openweathermap(object):
    def __init__(self, debug_level=0):
        self.api_q = 'guitrancourt'
        self.api_appid = "d510a6b5ef56136427985d8b0b3de77f"
        self.api_unit = "metric"
        self.api_lang = "fr"
        self.api_mode = 'json'

        self.api_url = 'http://api.openweathermap.org/data/2.5/weather'
        self.api_url_opt = '?'
        self.api_url_opt += 'appid=%s' % self.api_appid
        self.api_url_opt += '&'
        self.api_url_opt += 'q=%s' % self.api_q
        self.api_url_opt += '&'
        self.api_url_opt += 'lang=%s' % self.api_lang
        self.api_url_opt += '&'
        self.api_url_opt += 'unit=%s' % self.api_unit
        self.api_url_opt += '&'
        self.api_url_opt += 'mode=%s' % self.api_mode

        yaml_dir = u''
        yaml_dir += u''.join(os.path.dirname(os.path.realpath(__file__)))
        yaml_dir += u''.join(os.sep)
        yaml_dir += u'api'
        yaml_dir += u''.join(os.sep)
        self.api = yaml.load(open(yaml_dir + "openweather.yml", 'r'))
        # Exec
        if debug_level >= 2:
            sys.stdout.write(unicode('OpenWeatherMap Information\'s:\n'))
            sys.stdout.write(unicode('----------------------------'))
            sys.stdout.write(unicode('\n'))
            sys.stdout.flush()

        self.data = self.get_information(debug_level=debug_level)
        self.read_information(debug_level=debug_level)

    def read_information(self, debug_level=0):
        # if 'cod' in self.data:
        #     if 'message' in self.data:
        #         print self.data['message']

        for original_key in self.api['keys_defaulting_to_unknown']:
            self.api['weather_data_api'][original_key]['value'] = self.data.get(original_key, "Unknown")

        for original_key in self.api['keys_defaulting_to_unknown_encoded']:
            self.api['weather_data_api'][original_key]['value'] = self.data.get(original_key, "Unknown").encode('utf-8')

        for weather_idx in range(0, 6):
            for orig_key in self.api['keys_weathers']:
                self.api['weather_data_api']['weather'][weather_idx][orig_key]['value'] = self.data.get(
                    'weather_' + str(weather_idx) + "_" + orig_key, "Unknown")

        self.api['weather_data_api']['rain_1h']['value'] = self.data.get('rain_1h', 0)
        self.api['weather_data_api']['rain_3h']['value'] = self.data.get('rain_3h', 0)
        self.api['weather_data_api']['snow_1h']['value'] = self.data.get('snow_1h', 0)
        self.api['weather_data_api']['snow_3h']['value'] = self.data.get('snow_3h', 0)

    def get_information(self, debug_level=0):
        req = self.api_url + self.api_url_opt

        # DEBUG Infos
        if debug_level >= 1:
            sys.stdout.write(unicode('Url: '))
            sys.stdout.write(unicode(req))
            sys.stdout.write(unicode('\n'))
            sys.stdout.flush()

        # Care about network error
        try:
            request = urlopen(req)
        except HTTPError as e:
            # DEBUG Infos
            if debug_level >= 1:
                sys.stdout.write(unicode('The server couldn\'t fulfill the request.'))
                sys.stdout.write(unicode('\n'))
                sys.stdout.write(unicode('Error code: '))
                sys.stdout.write(e.code)
                sys.stdout.write(unicode('\n'))
                sys.stdout.flush()
            sys.exit(1)
        except URLError as e:
            # DEBUG Infos
            if debug_level >= 1:
                sys.stdout.write(unicode('We failed to reach a server.'))
                sys.stdout.write(unicode('\n'))
                sys.stdout.write(unicode('Reason: '))
                sys.stdout.write(e.reason)
                sys.stdout.write(unicode('\n'))
                sys.stdout.flush()
            sys.exit(1)
        else:
            data = request.read().decode('utf-8')

        # Care about json error
        try:
            data = json.loads(data)
        except ValueError, e:
            # DEBUG Infos
            if debug_level >= 1:
                sys.stdout.write(unicode('We failed to load json informations'))
                sys.stdout.write(unicode('\n'))
                sys.stdout.write(unicode('Message: '))
                sys.stdout.write(e.message)
                sys.stdout.write(unicode('\n'))
                sys.stdout.flush()
            sys.exit(1)
        else:
            data = self.flatten_dict(data)

            # DEBUG Infos
            if debug_level >= 2:
                sys.stdout.write(unicode(json.dumps(data, indent=4, sort_keys=True)))
                sys.stdout.write(unicode('\n'))
                sys.stdout.write(unicode('\n'))
                sys.stdout.flush()

            return data

    def flatten_dict(self, d, parent_key=''):
        """Recursively flatten a dict"""
        items = []
        for k, v in d.items():
            new_key = parent_key + '_' + k if parent_key else k
            if isinstance(v, collections.MutableMapping):
                items.extend(self.flatten_dict(v, new_key).items())
            elif type(v) == list:
                for n in range(len(v)):
                    mykey = "%s_%d" % (new_key, n)
                    items.extend(self.flatten_dict(v[n], mykey).items())
            else:
                items.append((new_key, v))
        return dict(items)
