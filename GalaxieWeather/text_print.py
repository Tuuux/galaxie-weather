#!/usr/bin/env python
# -*- coding: utf-8 -*-
from GalaxieWeather.text_convert import convert_f2c
from GalaxieWeather.text_convert import to_direction
from GalaxieWeather.text_convert import to_wind_description
import time
import os
import sys
import locale

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Jérôme ORNECH alias "Tuux" <tuxa@rtnp.org> all rights reserved
__author__ = 'Tuux'

locale.setlocale(locale.LC_TIME, '')


def print_7960_info(data):
    # max_length = 25

    # Icon file
    icon_path = u''
    icon_path += u''.join(os.path.dirname(os.path.realpath(__file__)))
    icon_path += u''.join(os.sep)
    icon_path += u'icons'
    icon_path += u''.join(os.sep)
    icon_path += u''.join(data.api['weather_data_api']['weather'][0]['icon']['value'])
    icon_path += u'.png'
    sys.stdout.write(unicode(icon_path))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Temperature
    temperature = unicode(u'')
    temperature += unicode(u'%.0f' % float(convert_f2c(data.api['weather_data_api']['main_temp']['value'])))
    temperature += unicode(data.api['weather_data_api']['main_temp']['small_unit'][str(data.api_unit)])
    sys.stdout.write(unicode(temperature))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Line 1
    line1 = unicode(u'')
    line1 += unicode(u'R:')
    if len(unicode(u'%.0f' % float(data.api['weather_data_api']['rain_3h']['value']))) < 2:
        line1 += unicode(' ')
    line1 += unicode(u'%.0f' % float(data.api['weather_data_api']['rain_3h']['value']))
    line1 += unicode(data.api['weather_data_api']['rain_3h']['small_unit'][str(data.api_unit)])
    sys.stdout.write(unicode(line1))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Line 2
    line2 = unicode(u'')
    line2 += unicode(u'H:')
    line2 += unicode(u'%.0f' % float(data.api['weather_data_api']['main_humidity']['value']))
    line2 += unicode(data.api['weather_data_api']['main_humidity']['unit'][str(data.api_unit)])
    sys.stdout.write(unicode(line2))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Line 3
    line3 = unicode(u'')
    line3 += unicode(u'P:')
    line3 += unicode(u'%.0f' % float(data.api['weather_data_api']['main_pressure']['value']))
    line3 += unicode(data.api['weather_data_api']['main_pressure']['unit'][str(data.api_unit)])
    sys.stdout.write(unicode(line3))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Line 4
    line4 = unicode(u'')
    line4 += unicode(to_wind_description(data.api['weather_data_api']['wind_speed']['value']))
    line4 += unicode(u' ')
    line4 += unicode(u'%.2f' % data.api['weather_data_api']['wind_speed']['value'])
    line4 += unicode(data.api['weather_data_api']['wind_speed']['small_unit'][str(data.api_unit)])
    sys.stdout.write(unicode(line4))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Line 5
    line5 = unicode(u'')
    line5 += unicode(to_direction(data.api['weather_data_api']['wind_deg']['value']))
    line5 += unicode(u' ')
    line5 += unicode(u'%.0f' % float(data.api['weather_data_api']['wind_deg']['value']))
    line5 += unicode(data.api['weather_data_api']['wind_deg']['micro_unit'][str(data.api_unit)])
    sys.stdout.write(unicode(line5))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Line 6
    weather_description_text = unicode(data.api['weather_data_api']['weather'][0]['description']['value'])
    weather_description_text = u' '.join(word[0].upper() + word[1:] for word in weather_description_text.split())
    line6 = unicode(u'')
    line6 += unicode(weather_description_text)
    line6 += unicode(u' ')
    line6 += unicode(data.api['weather_data_api']['clouds_all']['value'])
    line6 += unicode(data.api['weather_data_api']['clouds_all']['unit'][str(data.api_unit)])
    sys.stdout.write(unicode(line6))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Line 7
    line7 = unicode(u'')
    line7 += unicode(u'Aube')
    line7 += unicode(u' ')
    line7 += unicode(time.strftime('%H:%M', time.localtime(data.api['weather_data_api']['sys_sunrise']['value'])))
    line7 += unicode(u' ')
    line7 += unicode(u'Crépusc.')
    line7 += unicode(u' ')
    line7 += unicode(time.strftime('%H:%M', time.localtime(data.api['weather_data_api']['sys_sunset']['value'])))
    sys.stdout.write(unicode(line7))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()


def print_dump(data):
    # print (json.dumps(data.api['weather_data_api'], sort_keys=True, indent=4, separators=(',', ': ')))

    # City identification
    sys.stdout.write(unicode(data.api['weather_data_api']['id']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['id']['value']))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Data receiving time
    sys.stdout.write(unicode(data.api['weather_data_api']['dt']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['dt']['value']))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # City name
    sys.stdout.write(unicode(data.api['weather_data_api']['name']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['name']['value']))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # City geo location, latitude
    sys.stdout.write(unicode(data.api['weather_data_api']['coord_lat']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['coord_lat']['value']))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # City geo location, longitude
    sys.stdout.write(unicode(data.api['weather_data_api']['coord_lon']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['coord_lon']['value']))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # System parameter, do not use it
    sys.stdout.write(unicode(data.api['weather_data_api']['sys_message']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['sys_message']['value']))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Country code (GB, JP etc.)
    sys.stdout.write(unicode(data.api['weather_data_api']['sys_country']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['sys_country']['value']))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Sunrise time
    sys.stdout.write(unicode(data.api['weather_data_api']['sys_sunrise']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['sys_sunrise']['value']))
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['sys_sunrise']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Sunset time
    sys.stdout.write(unicode(data.api['weather_data_api']['sys_sunset']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['sys_sunset']['value']))
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['sys_sunset']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Temperature
    sys.stdout.write(unicode(data.api['weather_data_api']['main_temp']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(convert_f2c(data.api['weather_data_api']['main_temp']['value'])))
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['main_temp']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Humidity
    sys.stdout.write(unicode(data.api['weather_data_api']['main_humidity']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['main_humidity']['value']))
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['main_humidity']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Minimum temperature at the moment
    sys.stdout.write(unicode(data.api['weather_data_api']['main_temp_min']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(convert_f2c(data.api['weather_data_api']['main_temp_min']['value'])))
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['main_temp_min']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Maximum temperature at the moment
    sys.stdout.write(unicode(data.api['weather_data_api']['main_temp_max']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(convert_f2c(data.api['weather_data_api']['main_temp_max']['value'])))
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['main_temp_max']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Atmospheric pressure
    sys.stdout.write(unicode(data.api['weather_data_api']['main_pressure']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['main_pressure']['value']))
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['main_pressure']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Atmospheric pressure on the sea level
    sys.stdout.write(unicode(data.api['weather_data_api']['main_sea_level']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['main_sea_level']['value']))
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['main_sea_level']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Atmospheric pressure on the ground level
    sys.stdout.write(unicode(data.api['weather_data_api']['main_grnd_level']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['main_grnd_level']['value']))
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['main_grnd_level']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Wind speed
    sys.stdout.write(unicode(data.api['weather_data_api']['wind_speed']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['wind_speed']['value']))
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['wind_speed']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Wind direction
    sys.stdout.write(unicode(data.api['weather_data_api']['wind_deg']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['wind_deg']['value']))
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['wind_deg']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Wind gust
    sys.stdout.write(unicode(data.api['weather_data_api']['wind_gust']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['wind_gust']['value']))
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['wind_gust']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Cloudiness
    sys.stdout.write(unicode(data.api['weather_data_api']['clouds_all']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['clouds_all']['value']))
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['clouds_all']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Weather condition id
    sys.stdout.write(unicode(data.api['weather_data_api']['weather'][0]['id']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['weather'][0]['id']['value']))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Group of weather parameters
    sys.stdout.write(unicode(data.api['weather_data_api']['weather'][0]['main']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['weather'][0]['main']['value']))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Weather condition within the group
    sys.stdout.write(unicode(data.api['weather_data_api']['weather'][0]['description']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['weather'][0]['description']['value']))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Weather icon id
    sys.stdout.write(unicode(data.api['weather_data_api']['weather'][0]['icon']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['weather'][0]['icon']['value']))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Precipitation volume for last hour
    sys.stdout.write(unicode(data.api['weather_data_api']['rain_1h']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['rain_1h']['value']))
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['rain_1h']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # rain_3h - Precipitation volume for last 3h hours
    sys.stdout.write(unicode(data.api['weather_data_api']['rain_3h']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['rain_3h']['value']))
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['rain_3h']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # snow_1h - Snow volume for the last hour
    sys.stdout.write(unicode(data.api['weather_data_api']['snow_1h']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['snow_1h']['value']))
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['snow_1h']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # snow_3h - Snow volume for last 3 hours
    sys.stdout.write(unicode(data.api['weather_data_api']['snow_3h']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['snow_3h']['value']))
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['snow_3h']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()


def print_human_dump(data):
    # print (json.dumps(data.api['weather_data_api'], sort_keys=True, indent=4, separators=(',', ': ')))

    # Data receiving time
    sys.stdout.write(
        unicode(str(
            time.strftime('%A %d %B %Y %H:%M:%S',
                          time.localtime(data.api['weather_data_api']['dt']['value'])))).title())
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # City identification
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['id']['small_description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['id']['value']))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # City name
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['name']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['name']['value']))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # City geo location, latitude
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode('City Latitude'))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['coord_lat']['value']))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # City geo location, longitude
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode('City Longitude'))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['coord_lon']['value']))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Country code (GB, JP etc.)
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode('City Country'))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['sys_country']['value']))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Sunrise time
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['sys_sunrise']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(
        unicode(time.strftime('%H:%M', time.localtime(data.api['weather_data_api']['sys_sunrise']['value']))))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Sunset time
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['sys_sunset']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(
        unicode(time.strftime('%H:%M', time.localtime(data.api['weather_data_api']['sys_sunset']['value']))))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Temperature
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['main_temp']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(convert_f2c(data.api['weather_data_api']['main_temp']['value'])))
    sys.stdout.write(unicode(data.api['weather_data_api']['main_temp']['small_unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Humidity
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['main_humidity']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['main_humidity']['value']))
    sys.stdout.write(unicode(data.api['weather_data_api']['main_humidity']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Atmospheric pressure
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['main_pressure']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['main_pressure']['value']))
    sys.stdout.write(unicode(data.api['weather_data_api']['main_pressure']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Wind speed
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['wind_speed']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['wind_speed']['value']))
    sys.stdout.write(unicode(data.api['weather_data_api']['wind_speed']['small_unit'][str(data.api_unit)]))
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(to_wind_description(data.api['weather_data_api']['wind_speed']['value'])))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Wind direction
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['wind_deg']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(to_direction((data.api['weather_data_api']['wind_deg']['value']))))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Wind gust
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['wind_gust']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['wind_gust']['value']))
    sys.stdout.write(unicode(data.api['weather_data_api']['wind_gust']['small_unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Cloudiness
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['clouds_all']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['clouds_all']['value']))
    sys.stdout.write(unicode(data.api['weather_data_api']['clouds_all']['unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Weather condition id
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['weather'][0]['id']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['weather'][0]['id']['value']))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Group of weather parameters
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['weather'][0]['main']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['weather'][0]['main']['value']))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Weather condition within the group
    description_text = unicode(data.api['weather_data_api']['weather'][0]['description']['value'])
    description_text = ' '.join(word[0].upper() + word[1:] for word in description_text.split())
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['weather'][0]['description']['description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(description_text))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Weather icon id
    icon_path = u''
    icon_path += u''.join(os.path.dirname(os.path.realpath(__file__)))
    icon_path += u''.join(os.sep)
    icon_path += u'icons'
    icon_path += u''.join(os.sep)
    icon_path += u''.join(data.api['weather_data_api']['weather'][0]['icon']['value'])
    icon_path += u'.png'
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['weather'][0]['icon']['small_description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(icon_path))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # Precipitation volume for last hour
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['rain_1h']['small_description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['rain_1h']['value']))
    sys.stdout.write(unicode(data.api['weather_data_api']['rain_1h']['small_unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # rain_3h - Precipitation volume for last 3h hours
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['rain_3h']['small_description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['rain_3h']['value']))
    sys.stdout.write(unicode(data.api['weather_data_api']['rain_3h']['small_unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # snow_1h - Snow volume for the last hour
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['snow_1h']['small_description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['snow_1h']['value']))
    sys.stdout.write(unicode(data.api['weather_data_api']['snow_1h']['small_unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()

    # snow_3h - Snow volume for last 3 hours
    sys.stdout.write(unicode(' '))
    sys.stdout.write(unicode(data.api['weather_data_api']['snow_3h']['small_description']))
    sys.stdout.write(unicode(': '))
    sys.stdout.write(unicode(data.api['weather_data_api']['snow_3h']['value']))
    sys.stdout.write(unicode(data.api['weather_data_api']['snow_3h']['small_unit'][str(data.api_unit)]))
    sys.stdout.write(unicode('\n'))
    sys.stdout.flush()
