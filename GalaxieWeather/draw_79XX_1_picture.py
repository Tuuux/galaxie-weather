#!/usr/bin/env python
# -*- coding: utf-8 -*-

from GalaxieWeather.text_convert import convert_f2c
from GalaxieWeather.text_convert import to_direction
from GalaxieWeather.text_convert import to_wind_description
import time
import os
import locale
from PIL import Image, ImageDraw, ImageFont

locale.setlocale(locale.LC_TIME, '')
# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Jérôme ORNECH alias "Tuux" <tuxa@rtnp.org> all rights reserved
__author__ = 'Tuux'


def draw_7960_1_info(app_config, data, debug_level=0):

    # Icon file
    icon_file = app_config.icon_dir
    icon_file += u''.join(data.api['weather_data_api']['weather'][0]['icon']['value'])
    icon_file += u'.png'

    font_dir = app_config.font_dir

    # Temperature
    temperature = unicode(u'')
    temperature += unicode(u'%.0f' % float(convert_f2c(data.api['weather_data_api']['main_temp']['value'])))
    temperature += unicode(data.api['weather_data_api']['main_temp']['small_unit'][str(data.api_unit)])

    # Line 1
    line1 = unicode(u'')
    line1 += unicode(u'R:')
    if len(unicode(u'%.0f' % float(data.api['weather_data_api']['rain_3h']['value']))) < 2:
        line1 += unicode(' ')
    line1 += unicode(u'%.0f' % float(data.api['weather_data_api']['rain_3h']['value']))
    line1 += unicode(data.api['weather_data_api']['rain_3h']['small_unit'][str(data.api_unit)])

    # Line 2
    line2 = unicode(u'')
    line2 += unicode(u'H:')
    line2 += unicode(u'%.0f' % float(data.api['weather_data_api']['main_humidity']['value']))
    line2 += unicode(data.api['weather_data_api']['main_humidity']['unit'][str(data.api_unit)])

    # Line 3
    line3 = unicode(u'')
    line3 += unicode(u'P:')
    line3 += unicode(u'%.0f' % float(data.api['weather_data_api']['main_pressure']['value']))
    line3 += unicode(data.api['weather_data_api']['main_pressure']['unit'][str(data.api_unit)])

    # Line 4
    line4 = unicode(u'')
    line4 += unicode(to_wind_description(data.api['weather_data_api']['wind_speed']['value']))
    line4 += unicode(u' ')
    line4 += unicode(u'%.2f' % data.api['weather_data_api']['wind_speed']['value'])
    line4 += unicode(data.api['weather_data_api']['wind_speed']['small_unit'][str(data.api_unit)])

    # Line 5
    line5 = unicode(u'')
    line5 += unicode(to_direction(data.api['weather_data_api']['wind_deg']['value']))
    line5 += unicode(u' ')
    line5 += unicode(u'%.0f' % float(data.api['weather_data_api']['wind_deg']['value']))
    line5 += unicode(data.api['weather_data_api']['wind_deg']['micro_unit'][str(data.api_unit)])

    # Line 6
    weather_description_text = unicode(data.api['weather_data_api']['weather'][0]['description']['value'])
    weather_description_text = u' '.join(word[0].upper() + word[1:] for word in weather_description_text.split())
    line6 = unicode(u'')
    line6 += unicode(weather_description_text)
    line6 += unicode(u' ')
    line6 += unicode(data.api['weather_data_api']['clouds_all']['value'])
    line6 += unicode(data.api['weather_data_api']['clouds_all']['unit'][str(data.api_unit)])

    # Line 7
    line7 = unicode(u'')
    line7 += unicode(u'Aube')
    line7 += unicode(u' ')
    line7 += unicode(time.strftime('%H:%M', time.localtime(data.api['weather_data_api']['sys_sunrise']['value'])))
    line7 += unicode(u' ')
    line7 += unicode(u'Crépu.')
    line7 += unicode(u' ')
    line7 += unicode(time.strftime('%H:%M', time.localtime(data.api['weather_data_api']['sys_sunset']['value'])))

    # Create a black image
    max_x = 125
    max_y = 60
    line_number = 7
    line_spacing = 0.1
    line_h_size = (max_y / (line_number - line_spacing))

    image = Image.new('RGB', (max_x, max_y), (255, 255, 255))

    # ICON
    icon_image = Image.open(icon_file, 'r')
    image.paste(icon_image, (-2, -10))
    drawer = ImageDraw.Draw(image)

    font = ImageFont.truetype(filename=font_dir+'DejaVuSans.ttf', size=9)
    font_giant = ImageFont.truetype(filename=font_dir+'DejaVuSans-Bold.ttf', size=13)

    drawer.text((int(max_x / 3), -1), temperature, fill='black', font=font_giant)
    drawer.text((int(max_x / 1.75), -1), line1, fill='black', font=font)
    drawer.text((int(max_x / 1.75), int(line_h_size * 1) - 1), line2, fill='black', font=font)
    drawer.text((int(max_x / 1.75), int(line_h_size * 2) - 1), line3, fill='black', font=font)
    drawer.text((0, int(line_h_size * 3) - 1), line4, fill='black', font=font)
    drawer.text((0, int(line_h_size * 4) - 1), line5, fill='black', font=font)
    drawer.text((0, int(line_h_size * 5) - 1), line6, fill='black', font=font)
    drawer.text((0, int(line_h_size * 6) - 1), line7, fill='black', font=font)

    return image
