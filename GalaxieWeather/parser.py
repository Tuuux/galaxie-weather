#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import os


def check_if_file_exists(file):
    if not os.path.exists(file):
        raise argparse.ArgumentTypeError("{0} does not exist".format(file))
    return file


def make_parser():
    parser = argparse.ArgumentParser(
        description="Galaxie Weather",
        prog='GalaxieWeather.py',
        usage='%(prog)s [options]'
    )

    parser.add_argument('outputfile', action="store", nargs='?',
                        help="Output file name, the format will be CIP, if no output file name is given, the inputfile \
                        name will be use, with .cip extention. and store inside the working directory")
    parser.add_argument("-v", "--verbosity",
                        action="count",
                        default=0,
                        help="increase output verbosity, -v or -vv or -vvv are accepted"
                        )
    parser.add_argument('--human-dump',
                        dest='human_dump',
                        action='store_true',
                        help="print and translate collected information's"
                        )
    parser.add_argument('--dump',
                        dest='dump',
                        action='store_true',
                        help='print collected information\'s without any translation, see -vv for have more information\'s'
                        )
    parser.add_argument('--mode',
                        choices=('79XX', 'wallpaper'),
                        default='79XX')
    parser.add_argument('--theme',
                        dest='theme',
                        choices=('1', '2', '3'),
                        default='3',
                        help="Choise for theme number 1, 2, 3 , etc ..."
                        )
    parser.add_argument('--background-file',
                        type=check_if_file_exists,
                        nargs='?',
                        action="store",
                        help="Background Image source file",
                        dest="backgroundfile")
    parser.add_argument('--show',
                        dest='show',
                        action='store_true',
                        help='Show the generated image'
                        )
    return parser

