#!/usr/bin/env python
# -*- coding: utf-8 -*-
import ephem
import datetime
import sys
from math import degrees

# Clumsy way of extracting date from filename


def get_moon_infos(timestamp, lat, lon, pressure, debug_level=0):
    # Make an observer
    location = ephem.Observer()

    # Set a date
    location.date = datetime.datetime.utcfromtimestamp(timestamp)
    # location.date = ephem.Date(ephem.localtime(ephem.now()))

    # Location of Fredericton, Canada
    location.long = str(lon)  # Note that lat should be in string format
    location.lat = str(lat)  # Note that lat should be in string format

    # Elevation of Fredericton, Canada, in metres
    # location.elev = 20

    # Use these settings
    location.pressure = float(pressure)

    location.horizon = '-0:34'

    moon = ephem.Moon()
    moon.compute(location)
    # Declaration and collect
    actual_day_moonrise = location.next_rising(moon)
    actual_day_azimut = degrees(moon.az)
    actual_day_moonset = location.next_setting(moon)

    location.date = ephem.Date(location.date - (ephem.hour * 24))

    previous_day_azimut = degrees(moon.az)
    previous_day_moonrise = location.next_rising(moon)
    previous_day_moonset = location.next_setting(moon)

    # Start to compute the delta of actual_day and previous_day sunrise/sunset
    # Define a format it can be use by datetime
    fmt = '%Y-%m-%d %H:%M:%S'
    # Import ephem date to datetime
    previous_sunrise = datetime.datetime.strptime(
        '{0:%Y-%m-%d %H:%M:%S}'.format(ephem.localtime(previous_day_moonrise)), fmt)
    previous_sunset = datetime.datetime.strptime(
        '{0:%Y-%m-%d %H:%M:%S}'.format(ephem.localtime(previous_day_moonset)), fmt)
    # Compute dat

    previous_day_duration = (previous_sunset - previous_sunrise).total_seconds()
    # print previous_day_duration

    actual_sunrise = datetime.datetime.strptime('{0:%Y-%m-%d %H:%M:%S}'.format(ephem.localtime(actual_day_moonrise)), fmt)
    actual_sunset = datetime.datetime.strptime('{0:%Y-%m-%d %H:%M:%S}'.format(ephem.localtime(actual_day_moonset)), fmt)
    actual_day_duration = (actual_sunset - actual_sunrise).total_seconds()
    # print actual_day_duration


    if debug_level >= 2:
        sys.stdout.write(unicode('Moon Information\'s:\n'))
        sys.stdout.write(unicode('-----------------'))
        sys.stdout.write(unicode('\n'))

        localtime_text = '{0: <21}: {1:%Y-%m-%d %H:%M:%S}'.format('Localtime', ephem.localtime(location.date))
        sys.stdout.write(unicode(localtime_text))
        sys.stdout.write(unicode('\n'))

        actual_moonrise_text = '{0: <21}: {1:%Y-%m-%d %H:%M:%S}'.format('Moonrise', ephem.localtime(actual_day_moonrise))
        sys.stdout.write(unicode(actual_moonrise_text))
        sys.stdout.write(unicode('\n'))

        actual_day_moonset_text = '{0: <21}: {1:%Y-%m-%d %H:%M:%S}'.format('Moonset', ephem.localtime(actual_day_moonset))
        sys.stdout.write(unicode(actual_day_moonset_text))
        sys.stdout.write(unicode('\n'))

        previous_day_moonrise_text = '{0: <21}: {1:%Y-%m-%d %H:%M:%S}'.format('Moonrise Previous Day', ephem.localtime(previous_day_moonrise))
        sys.stdout.write(unicode(previous_day_moonrise_text))
        sys.stdout.write(unicode('\n'))

        previous_day_moonset_text = '{0: <21}: {1:%Y-%m-%d %H:%M:%S}'.format('Moonset Preview Day', ephem.localtime(previous_day_moonset))
        sys.stdout.write(unicode(previous_day_moonset_text))
        sys.stdout.write(unicode('\n'))

        azimut_text = '{0: <21}: {1:}'.format('Azimut', int(actual_day_azimut - previous_day_azimut))
        sys.stdout.write(unicode(azimut_text))
        sys.stdout.write(unicode('\n'))
        sys.stdout.flush()
        sys.stdout.flush()

    # Prepare return value
    return_moonrise_text = u''.join('{0:%H:%M}'.format(ephem.localtime(actual_day_moonrise)))
    return_moonset_text = u''.join('{0:%H:%M}'.format(ephem.localtime(actual_day_moonset)))
    return_delta_text = u''.join('{0:}'.format(int(actual_day_azimut - previous_day_azimut)))
    #return_delta_text += u'°'
    returned_dict = {u'Moonrise': return_moonrise_text, u'Moonset': return_moonset_text, u'Azimut': return_delta_text}

    if debug_level >= 2:
        returned_dic_text = '{0: <21}:'.format('Returned Value')
        sys.stdout.write(returned_dic_text)
        sys.stdout.write(unicode('\n'))
        sys.stdout.write(unicode(returned_dict))
        sys.stdout.write(unicode('\n'))
        sys.stdout.write(unicode('\n'))
        sys.stdout.flush()

    return returned_dict

def get_sun_infos(timestamp, lat, lon, pressure, debug_level=0):
    # Make an observer
    location = ephem.Observer()

    # Set a date
    location.date = datetime.datetime.utcfromtimestamp(timestamp)
    # location.date = ephem.Date(ephem.localtime(ephem.now()))

    # Location of Fredericton, Canada
    location.long = str(lon)  # Note that lat should be in string format
    location.lat = str(lat)  # Note that lat should be in string format

    # Elevation of Fredericton, Canada, in metres
    # location.elev = 20

    # To get U.S. Naval Astronomical Almanac values, use these settings
    location.pressure = float(pressure)

    location.horizon = '-0:34'

    # Declaration and collect
    actual_day_sunrise = location.next_rising(ephem.Sun())
    previous_day_sunrise = location.next_rising(ephem.Sun(), start=ephem.Date(location.date - (ephem.hour * 24)))
    actual_day_noon = location.next_transit(ephem.Sun(), start=actual_day_sunrise)
    actual_day_sunset = location.next_setting(ephem.Sun())
    previous_day_sunset = location.next_setting(ephem.Sun(), start=ephem.Date(location.date - (ephem.hour * 24)))

    # We relocate the horizon to get twilight times
    # -6=civil twilight, -12=nautical, -18=astronomical
    location.horizon = '-6'
    actual_day_twilight_beg = location.previous_rising(ephem.Sun(), use_center=True)
    actual_day_twilight_end = location.next_setting(ephem.Sun(), use_center=True)

    # Start to compute the delta of actual_day and previous_day sunrise/sunset
    # Define a format it can be use by datetime
    fmt = '%Y-%m-%d %H:%M:%S'
    # Import ephem date to datetime
    previous_sunrise = datetime.datetime.strptime(
        '{0:%Y-%m-%d %H:%M:%S}'.format(ephem.localtime(previous_day_sunrise)), fmt)
    previous_sunset = datetime.datetime.strptime(
        '{0:%Y-%m-%d %H:%M:%S}'.format(ephem.localtime(previous_day_sunset)), fmt)
    # Compute dat

    previous_day_duration = (previous_sunset - previous_sunrise).total_seconds()
    # print previous_day_duration

    actual_sunrise = datetime.datetime.strptime('{0:%Y-%m-%d %H:%M:%S}'.format(ephem.localtime(actual_day_sunrise)), fmt)
    actual_sunset = datetime.datetime.strptime('{0:%Y-%m-%d %H:%M:%S}'.format(ephem.localtime(actual_day_sunset)), fmt)
    actual_day_duration = (actual_sunset - actual_sunrise).total_seconds()
    # print actual_day_duration

    # delta_in_sec = previous_day_duration - actual_day_duration
    delta_in_sec = actual_day_duration - previous_day_duration
    sign = ''
    if delta_in_sec > 0:
        sign = '+'
    m, s = divmod(delta_in_sec, 60)
    delta_value = sign + "%02d:%02d" % (m, s)

    if debug_level >= 2:
        sys.stdout.write(unicode('Sun Information\'s:\n'))
        sys.stdout.write(unicode('-----------------'))
        sys.stdout.write(unicode('\n'))

        localtime_text = '{0: <21}: {1:%Y-%m-%d %H:%M:%S}'.format('Localtime', ephem.localtime(location.date))
        sys.stdout.write(unicode(localtime_text))
        sys.stdout.write(unicode('\n'))

        actual_day_twilight_beg_text = '{0: <21}: {1:%Y-%m-%d %H:%M:%S}'.format('Twilight Begin', ephem.localtime(actual_day_twilight_beg))
        sys.stdout.write(unicode(actual_day_twilight_beg_text))
        sys.stdout.write(unicode('\n'))

        actual_sunrise_text = '{0: <21}: {1:%Y-%m-%d %H:%M:%S}'.format('Sunrise', ephem.localtime(actual_day_sunrise))
        sys.stdout.write(unicode(actual_sunrise_text))
        sys.stdout.write(unicode('\n'))

        actual_day_noon_text = '{0: <21}: {1:%Y-%m-%d %H:%M:%S}'.format('Noon', ephem.localtime(actual_day_noon))
        sys.stdout.write(unicode(actual_day_noon_text))
        sys.stdout.write(unicode('\n'))

        actual_day_sunset_text = '{0: <21}: {1:%Y-%m-%d %H:%M:%S}'.format('Sunset', ephem.localtime(actual_day_sunset))
        sys.stdout.write(unicode(actual_day_sunset_text))
        sys.stdout.write(unicode('\n'))

        actual_day_twilight_end_text = '{0: <21}: {1:%Y-%m-%d %H:%M:%S}'.format('Twilight End', ephem.localtime(actual_day_twilight_end))
        sys.stdout.write(unicode(actual_day_twilight_end_text))
        sys.stdout.write(unicode('\n'))

        previous_day_sunset_text = '{0: <21}: {1:%Y-%m-%d %H:%M:%S}'.format('Sunset Preview Day', ephem.localtime(previous_day_sunset))
        sys.stdout.write(unicode(previous_day_sunset_text))
        sys.stdout.write(unicode('\n'))

        previous_day_sunrise_text = '{0: <21}: {1:%Y-%m-%d %H:%M:%S}'.format('Sunrise Previous Day', ephem.localtime(previous_day_sunrise))
        sys.stdout.write(unicode(previous_day_sunrise_text))
        sys.stdout.write(unicode('\n'))

        delta_text = '{0: <21}: {1:}'.format('Delta', delta_value)
        sys.stdout.write(unicode(delta_text))
        sys.stdout.write(unicode('\n'))
        sys.stdout.flush()

    # Prepare return value
    return_sunrise_text = u''.join('{0:%H:%M}'.format(ephem.localtime(actual_day_sunrise)))
    return_sunset_text = u''.join('{0:%H:%M}'.format(ephem.localtime(actual_day_sunset)))
    return_delta_text = u''.join('{0:}'.format(delta_value))
    returned_dict = {u'Sunrise': return_sunrise_text, u'Sunset': return_sunset_text, u'Delta': return_delta_text}

    if debug_level >= 2:
        returned_dic_text = '{0: <21}:'.format('Returned Value')
        sys.stdout.write(returned_dic_text)
        sys.stdout.write(unicode('\n'))
        sys.stdout.write(unicode(returned_dict))
        sys.stdout.write(unicode('\n'))
        sys.stdout.write(unicode('\n'))
        sys.stdout.flush()

    return returned_dict

def get_day_or_night_for_wi(timestamp, lat, lon, pressure, debug_level=0):
    location = ephem.Observer()
    location.date = datetime.datetime.utcfromtimestamp(timestamp)
    location.long = str(lon)
    location.lat = str(lat)
    location.pressure = float(pressure)

    # Declaration and collect
    next_sunrise_datetime = location.next_rising(ephem.Sun())
    next_sunset_datetime = location.next_setting(ephem.Sun())

    it_is_day = next_sunset_datetime < next_sunrise_datetime
    #it_is_night = next_sunrise_datetime < next_sunset_datetime

    if it_is_day:
        return str('day')
    else:
        return str('night')


def get_moon_phase_now():
    # The Work
    date = ephem.Date(ephem.localtime(ephem.now()))
    nnm = ephem.next_new_moon(date)
    pnm = ephem.previous_new_moon(date)
    lunation = (date - pnm) / (nnm - pnm)
    return float(lunation)


def to_moon_day(lunation):
    moon_days_number = 28.0 + 1.53058796296
    value_for_a_day = lunation * moon_days_number
    # We return 29d 12h 44 min 2,8s by add 1.53058796296 to value_for_a_day
    # 1.53058796296 day ~=  1 day 12 hr. 44 min. 2 sec. 800 ms
    black_moon_days_number = 1.53058796296
    # return int(value_for_a_day + black_moon_days_number)
    return int(value_for_a_day)


def get_phase_on_day(year, month, day, lat, lon, temp, pressure):
    # Clean variables in case
    year = int(year)
    month = int(month)
    day = int(day)
    # Deal with location
    location = ephem.Observer()
    location.long = str(lon)
    location.lat = str(lat)
    location.temp = float(temp)
    location.pressure = float(pressure)
    location.date = ephem.Date(datetime.date(year, month, day))

    # The Work
    date = location.date
    nnm = ephem.next_new_moon(date)
    pnm = ephem.previous_new_moon(date)
    lunation = (date - pnm) / (nnm - pnm)
    return float(lunation)
