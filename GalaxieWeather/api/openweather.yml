---
# http://openweathermap.org/weather-data#current

keys_weathers:
  - id
  - main
  - description
  - icon

keys_defaulting_to_unknown:
  - id
  - dt
  - coord_lat
  - coord_lon
  - sys_message
  - sys_sunrise
  - sys_sunset
  - main_temp
  - main_humidity
  - main_temp_min
  - main_temp_max
  - main_pressure
  - main_sea_level
  - main_grnd_level
  - wind_speed
  - wind_deg
  - wind_gust
  - clouds_all

keys_defaulting_to_unknown_encoded:
  - name
  - sys_country

weather_data_api:
  id:
    description: 'City identification'
    small_description: 'City ID'
    unit: { standard: '-', metric: '-', imperial: '-' }
  dt:
    description: 'Data receiving time'
    small_description: 'Receiving time'
    unit: { standard: 'unix, UTC', metric: 'unix, UTC', imperial: 'unix, UTC' }
  name:
    description: 'City Name'
    small_description: 'City Name'
    unit: { standard: '-', metric: '-', imperial: '-' }
  coord_lat:
    description: 'City geo location, latitude'
    unit: { standard: '-', metric: '-', imperial: '-' }
  coord_lon:
    description: 'City geo location, longitude'
    unit: { standard: '-', metric: '-', imperial: '-' }
  sys_message:
    description: 'System parameter, do not use it'
    unit: { standard: '-', metric: '-', imperial: '-' }
  sys_country:
    description: 'Country code (GB, JP etc.)'
    unit: { standard: '-', metric: '-', imperial: '-' }
  sys_sunrise:
    description: 'Sunrise time'
    unit: { standard: 'unix, UTC', metric: 'unix, UTC', imperial: 'unix, UTC' }
  sys_sunset:
    description: 'Sunset time'
    unit: { standard: 'unix, UTC', metric: 'unix, UTC', imperial: 'unix, UTC' }
  main_humidity:
    description: 'Humidity'
    unit: { standard: '%', metric: '%', imperial: '%' }
  main_temp:
    description: 'Temperature'
    unit: { standard: 'Kelvin', metric: 'Celsius', imperial: 'Fahrenheit' }
    small_unit: { standard: '°K', metric: '°C', imperial: '°F' }
  main_temp_min:
    description: 'Minimum temperature at the moment'
    unit: { standard: 'Kelvin', metric: 'Celsius', imperial: 'Fahrenheit' }
    unit_short: { standard: '°K', metric: '°C', imperial: '°F' }
  main_temp_max:
    description: 'Maximum temperature at the moment'
    unit: { standard: 'Kelvin', metric: 'Celsius', imperial: 'Fahrenheit' }
    unit_short: { standard: '°K', metric: '°C', imperial: '°F' }
  main_pressure:
    description: 'Atmospheric pressure'
    unit: { standard: 'hPa', metric: 'hPa', imperial: 'hPa' }
  main_sea_level:
    description: 'Atmospheric pressure on the sea level'
    unit: { standard: 'hPa', metric: 'hPa', imperial: 'hPa' }
  main_grnd_level:
    description: 'Atmospheric pressure on the ground level'
    unit: { standard: 'hPa', metric: 'hPa', imperial: 'hPa' }
  wind_speed:
    description: 'Wind speed'
    unit: { standard: 'meter/sec', metric: 'meter/sec', imperial: 'meter/hour' }
    small_unit: { standard: 'm/s', metric: 'm/s', imperial: '°m/h' }
  wind_deg:
    description: 'Wind direction'
    unit: { standard: 'degrees (meteorological)', metric: 'degrees (meteorological)', imperial: 'degrees (meteorological)' }
    small_unit: { standard: '° (meteorological)', metric: '° (meteorological)', imperial: '° (meteorological)' }
    micro_unit: { standard: '°', metric: '°', imperial: '°' }
  wind_gust:
    description: 'Wind gust'
    unit: { standard: 'meter/sec', metric: 'meter/sec', imperial: 'meter/hour' }
    small_unit: { standard: 'm/s', metric: 'm/s', imperial: '°m/h' }
  clouds_all:
    description: 'Cloudiness'
    unit: { standard: '%', metric: '%', imperial: '%' }
  sys_message:
    description: 'System parameter, do not use it'
    unit: { standard: '-', metric: '-', imperial: '-' }
  rain_1h:
    description: 'Precipitation volume for the last hour'
    small_description: 'Precipitation volume'
    unit: { standard: 'mn', metric: 'mn', imperial: 'mn' }
    long_unit: { standard: 'mn/hour', metric: 'mn/hour', imperial: 'mn/hour' }
    small_unit: { standard: 'mn/h', metric: 'mn/h', imperial: 'mn/h' }
  rain_3h:
    description: 'Precipitation volume for the last 3 hours'
    small_description: 'Precipitation volume'
    unit: { standard: 'mn', metric: 'mn', imperial: 'mn' }
    long_unit: { standard: 'mn/3hours', metric: 'mn/3hours', imperial: 'mn/3hours' }
    small_unit: { standard: 'mn/3h', metric: 'mn/3h', imperial: 'mn/3h' }
  snow_1h:
    description: 'Snow volume for the last hour'
    small_description: 'Snow volume'
    unit: { standard: 'mn', metric: 'mn', imperial: 'mn' }
    long_unit: { standard: 'mn/hour', metric: 'mn/hour', imperial: 'mn/hour' }
    small_unit: { standard: 'mn/h', metric: 'mn/h', imperial: 'mn/h' }
  snow_3h:
    description: 'Snow volume for the last 3 hours'
    small_description: 'Snow volume'
    unit: { standard: 'mn', metric: 'mn', imperial: 'mn' }
    long_unit: { standard: 'mn/3hours', metric: 'mn/3hours', imperial: 'mn/3hours' }
    small_unit: { standard: 'mn/3h', metric: 'mn/3h', imperial: 'mn/3h' }
  weather:
    - id:
        description: 'Weather condition id'
        unit: { standard: '-', metric: '-', imperial: '-' }
      main:
        description: 'Group of weather parameters'
        unit: { standard: '-', metric: '-', imperial: '-' }
      description:
        description: 'Weather condition within the group'
        unit: { standard: '-', metric: '-', imperial: '-' }
      icon:
        description: 'Weather icon id'
        small_description: 'Icon'
        unit: { standard: '-', metric: '-', imperial: '-' }
    - id:
        description: 'Weather condition id'
        unit: { standard: '-', metric: '-', imperial: '-' }
      main:
        description: 'Group of weather parameters'
        unit: { standard: '-', metric: '-', imperial: '-' }
      description:
        description: 'Weather condition within the group'
        unit: { standard: '-', metric: '-', imperial: '-' }
      icon:
        description: 'Weather icon id'
        small_description: 'Icon'
        unit: { standard: '-', metric: '-', imperial: '-' }
    - id:
        description: 'Weather condition id'
        unit: { standard: '-', metric: '-', imperial: '-' }
      main:
        description: 'Group of weather parameters'
        unit: { standard: '-', metric: '-', imperial: '-' }
      description:
        description: 'Weather condition within the group'
        unit: { standard: '-', metric: '-', imperial: '-' }
      icon:
        description: 'Weather icon id'
        small_description: 'Icon'
        unit: { standard: '-', metric: '-', imperial: '-' }
    - id:
        description: 'Weather condition id'
        unit: { standard: '-', metric: '-', imperial: '-' }
      main:
        description: 'Group of weather parameters'
        unit: { standard: '-', metric: '-', imperial: '-' }
      description:
        description: 'Weather condition within the group'
        unit: { standard: '-', metric: '-', imperial: '-' }
      icon:
        description: 'Weather icon id'
        small_description: 'Icon'
        unit: { standard: '-', metric: '-', imperial: '-' }
    - id:
        description: 'Weather condition id'
        unit: { standard: '-', metric: '-', imperial: '-' }
      main:
        description: 'Group of weather parameters'
        unit: { standard: '-', metric: '-', imperial: '-' }
      description:
        description: 'Weather condition within the group'
        unit: { standard: '-', metric: '-', imperial: '-' }
      icon:
        description: 'Weather icon id'
        small_description: 'Icon'
        unit: { standard: '-', metric: '-', imperial: '-' }
    - id:
        description: 'Weather condition id'
        unit: { standard: '-', metric: '-', imperial: '-' }
      main:
        description: 'Group of weather parameters'
        unit: { standard: '-', metric: '-', imperial: '-' }
      description:
        description: 'Weather condition within the group'
        unit: { standard: '-', metric: '-', imperial: '-' }
      icon:
        description: 'Weather icon id'
        small_description: 'Icon'
        unit: { standard: '-', metric: '-', imperial: '-' }
    - id:
        description: 'Weather condition id'
        unit: { standard: '-', metric: '-', imperial: '-' }
      main:
        description: 'Group of weather parameters'
        unit: { standard: '-', metric: '-', imperial: '-' }
      description:
        description: 'Weather condition within the group'
        unit: { standard: '-', metric: '-', imperial: '-' }
      icon:
        description: 'Weather icon id'
        small_description: 'Icon'
        unit: { standard: '-', metric: '-', imperial: '-' }





