#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import argparse

sys.path.insert(0, os.path.abspath('..'))
from GalaxieWeather.openweathermap import Openweathermap
from GalaxieWeather.text_print import print_dump
from GalaxieWeather.text_print import print_human_dump
from GalaxieWeather.img2cip import img_to_cip_data
from GalaxieWeather.img2cip import save_cip_file
from GalaxieWeather.text_print import print_7960_info
from GalaxieWeather.draw_79XX_1_picture import draw_7960_1_info
from GalaxieWeather.draw_79XX_2_picture import draw_7960_2_info
from GalaxieWeather.draw_79XX_3_picture import draw_7960_3_info
from GalaxieWeather.draw_wallpaper_1 import wallpaper_1
from GalaxieWeather.parser import make_parser
from GalaxieWeather.app_init import AppInit
# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Jérôme ORNECH alias "Tuux" <tuxa@rtnp.org> all rights reserved
__author__ = 'Tuux'


if __name__ == "__main__":
    app_config = AppInit()

    debug_level = 0
    human_dump = 0

    parser = make_parser()
    args = parser.parse_args()

    if args.backgroundfile:
        background_file_ext = os.path.splitext(args.backgroundfile)[1]
        background_file_name = os.path.basename(os.path.splitext(args.backgroundfile)[0])
        background_file_working_dir = os.path.realpath(os.path.dirname(args.backgroundfile))
        background_file = os.path.join(background_file_working_dir, background_file_name + background_file_ext)
    else:
        background_file = 0

    output_file_ext = '.png'
    output_file_name = 'out'
    output_file_working_dir = os.getcwd()
    output_file = os.path.join(output_file_working_dir, output_file_name + output_file_ext)

    if args.outputfile:
        output_file_ext = os.path.splitext(args.outputfile)[1]
        output_file_name = os.path.basename(os.path.splitext(args.outputfile)[0])
        output_file_working_dir = os.path.realpath(os.path.dirname(args.outputfile))
        output_file = os.path.join(output_file_working_dir, output_file_name + output_file_ext)

    if args.verbosity:
        app_config.debug_level = args.verbosity

    weather_info = Openweathermap(debug_level=app_config.debug_level)

    if args.human_dump:
        print_human_dump(weather_info)
    if args.dump:
        print_dump(weather_info)
    #print_7960_info(meteo)
    #draw_7960_1_info(meteo)

    if args.mode == 'wallpaper':
        if args.theme == '1':
            image = wallpaper_1(app_config,
                                weather_info,
                                background_file=background_file,
                                debug_level=app_config.debug_level
                                )
        else:
            image = wallpaper_1(app_config,
                                weather_info,
                                background_file=background_file,
                                debug_level=app_config.debug_level
                                )
        image.save(os.path.abspath(output_file), 'PNG', quality=10)
        # os.system(
        #     "gsettings set org.gnome.desktop.background picture-uri file://" + os.path.abspath(output_file)
        # )
    elif args.mode == '79XX':
        if args.theme == '1':
            image = draw_7960_1_info(app_config,
                                     weather_info,
                                     debug_level=app_config.debug_level
                                     )
        elif args.theme == '2':
            image = draw_7960_2_info(app_config,
                                     weather_info,
                                     debug_level=app_config.debug_level
                                     )
        elif args.theme == '3':
            image = draw_7960_3_info(app_config,
                                     weather_info,
                                     debug_level=app_config.debug_level
                                     )
        else:
            image = draw_7960_2_info(app_config,
                                     weather_info,
                                     debug_level=app_config.debug_level
                                     )
        width, height = image.size

        cip_data = img_to_cip_data(
            image.convert('RGB'),
            debug_level=app_config.debug_level
        )
        save_cip_file(
            filename_path='out.cip',
            height=height,
            width=width,
            cip_data=cip_data,
            overwrite=1,
            debug_level=app_config.debug_level
        )

    if args.show:
        image.show()

    # THE END
    sys.exit(0)



